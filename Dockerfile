ARG IMAGE

FROM $IMAGE

RUN apt-get update && \
	apt-get install -y curl wget procps zip git g++ pkg-config \
	libssl-dev zlib1g-dev libicu-dev libpq-dev
	
ARG UID=1000
RUN useradd -u $UID -g www-data -m -s /bin/bash developer
