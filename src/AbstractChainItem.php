<?php
namespace marcoc\input;

abstract class AbstractChainItem
{
	/**
	 * 
	 * @param string $name
	 * @param mixed $value
	 * @throws \Error
	 * @return \marcoc\input\Validator\AbstractValidator
	 */
	public function setOption( string $name , $value )
	{
		$const = get_class($this)."::ATTR_".strtoupper($name);
		
		if( ! defined($const) ){
			throw new \Error( get_class($this) . ' has not property '.$name);
		}
		
		$this->{ constant($const) } = $value;
		return $this;
	}
	
	/**
	 * 
	 * @param string $name
	 * @throws \Error
	 * @return mixed
	 */
	public function getOption( string $name )
	{
		$const = get_class($this)."::ATTR_".strtoupper($name);
		
		if( ! defined($const) ){
			throw new \Error( get_class($this) . ' has not property '.$name);
		}
		
		return $this->{ constant($const) };
	}
}

