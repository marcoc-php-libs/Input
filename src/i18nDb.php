<?php
namespace marcoc\input;

class i18nDb {

    static private $db;

    static public function setDb( array $db ){
        self::$db = $db;
    }

    static public function getDb()
    {
        return self::$db;
    }
}

i18nDb::setDb( (include __DIR__ . '/../resources/i18n_database.php')['list'] );