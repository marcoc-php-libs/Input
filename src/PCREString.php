<?php
namespace marcoc\input;

class PCREString
{
	public $pattern;
	
	/**
	 * 
	 * @param string $pattern
	 */
	public function __construct( string $pattern )
	{
		$this->pattern = $pattern;
	}
	
	public function __toString()
	{
		return $this->pattern;
	}
}

