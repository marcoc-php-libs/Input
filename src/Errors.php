<?php
namespace marcoc\input;

class Errors
{
	/**
	 * 
	 * @var array[Error]
	 */
	public $list = [];
	
	public function add( Error $Error )
	{
		$this->list[] = $Error;
	}
	
	public function mergeErrors( Errors $Errors , string $namespace )
	{
		foreach ($Errors->list as $Error ){
			$Error->full_name = $namespace.'.'.$Error->full_name;
			$this->list[] = $Error;
		}
		
		return $this;
	}
}

