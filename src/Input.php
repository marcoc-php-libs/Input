<?php
namespace marcoc\input;


class Input
{
	
	/**
	 * Var is processed as array of item. If var is not array return false
	 * @var string
	 */
	CONST PROCESS_AS_ARRAY_ALWAYS = 'ALWAYS';
	/**
	 * Var is processed as array if is array, as single var if isn't array.
	 * Be carefoul! If var item expected is array, this schema will have an unexpected result
	 * @var string
	 */
	CONST PROCESS_AS_ARRAY_IF = 'IF';
	/**
	 * Var is processed as array if is theoretic array, as single var if isn't.
	 * @var string
	 */
	CONST PROCESS_AS_ARRAY_IF_THEOR_ARRAY = 'IF_THEOR_ARRAY';
	/**
	 * Var is processed as array if is theoretic object, as single var if isn't.
	 * @var string
	 */
	CONST PROCESS_AS_ARRAY_IF_THEOR_OBJECT = 'IF_THEOR_OBJECT';
	/**
	 * DEFAULT, item is processed as single item
	 * @var string
	 */
	CONST PROCESS_AS_ARRAY_NONE = 'NONE';
	
	/**
	 * some name of item array key
	 * @var string
	 */
	public $name;

	protected $array_index = null;
	
	/**
	 * Chech if item exists with "array_key_exists($name , $input) === true" if false
	 * and required = true return false
	 * @var boolean
	 */
	public $required = true;
	
	protected $process_chain = [];
	
	/**
	 * @var array[Input]
	 */
	protected $childs = [];
	private $Parent;
	
	/**
	 * @var string set process array mode
	 */
	public $process_as_array = self::PROCESS_AS_ARRAY_NONE;

	/**
	 * @var integer if process as array is enabled, only childs start from this index are processed
	 * as array. Childs before are processed normally
	 */
	public $process_as_array_from_child_index = 0;
	
	/**
	 * @var string set childs process array mode
	 */
	public $process_childs_as_array = self::PROCESS_AS_ARRAY_NONE;
	
	/**
	 * @var boolean set if stop process chain whene an item return a null value
	 */
	public $stop_if_null = true;
	/**
	 * @var boolean set if stop process chain whene an item return an empty value
	 */
	public $stop_if_empty = false;
	
	/**
	 * @var boolean set if destroy array element with some item name when after processing result null
	 */
	public $unset_if_null = false;
	/**
	 * @var boolean set if destroy array element with some item name when after processing result empty
	 */
	public $unset_if_empty = false;
	
	/**
	 * @var boolean if true will use $default_if_not_exists value whene index not exists
	 */
	private $use_default_if_not_exists = false;
	private $default_if_not_exists;
	
	/**
	 * @var boolean set if continue after a process chain return false
	 */
	public $continue_on_error = false;
	/**
	 * @var array set how Input childs run
	 */
	public $validation_group = [];
	
	private $source_data;
	private $data;
	
	/**
	 * 
	 * @deprecated 1.3.0
	 */
	public $shared_storage;
	
	private $SharedStorage;
	
	/**
	 * @var Errors
	 */
	public $Errors;
	
	public function __construct( $name , $required , $process_chain = [] )
	{
		$this->name = $name;
		$this->required = $required;
		$this->process_chain = $process_chain;
		
		$this->initSharedStorage();
		
		$this->Errors = new Errors();
	}
	
	public function setValidationGroup( array $validation_group )
	{
		$this->validation_group = $validation_group;
		return $this;
	}
	
	public function setDefaultIfNotExists( $value )
	{
		$this->use_default_if_not_exists = true;
		$this->default_if_not_exists = $value;
		return $this;
	}
	
	public function unsetDefaultIfNotExists()
	{
		$this->use_default_if_not_exists = false;
		return $this;
	}
	
	public function getDefaultIfNotExists()
	{
		return $this->default_if_not_exists;
	}
	
	public function hasUseDefaultIfNotExists()
	{
		return $this->use_default_if_not_exists;
	}
	
	public function setData( $data )
	{
		$this->source_data = $data;
		$this->data = [];
		return $this;
	}
	
	public function getFullData()
	{
		return $this->source_data;
	}
	
	public function getData()
	{
		return $this->data;
	}

	/**
	 * Add new Validator or Filter to chain
	 * @param \marcoc\input\Filter\FilterInterface|\marcoc\input\Input\Validator\ValidatorInterface|\marcoc\input\ChainModifier $chain_item
	 * @return \marcoc\input\Input
	 */
	public function addToChain( $chain_item )
	{
		$this->process_chain[] = $chain_item;
		return $this;
	}
	
	/**
	 * Add new Input
	 * @param Input $input
	 * @return \marcoc\input\Input
	 */
	public function addChild( Input $Input )
	{
		$this->childs[$Input->name] = $Input;
		$Input->Parent = $this;
		$Input->shared_storage = &$this->shared_storage;
		return $this;
	}
	
	/**
	 * @return array
	 */
	public function getChain()
	{
		return $this->process_chain;
	}
	
	/**
	 * @return array[\marcoc\input\Input]
	 */
	public function getChilds()
	{
		return $this->childs;
	}
	
	/**
	 * 
	 * @param string $name
	 * @return \marcoc\input\Input
	 */
	public function get( string $name )
	{
		return $this->childs[$name] ?? null;
	}
	
	/**
	 * @deprecated use setParent()
	 */
	protected function setParet( Input $Input )
	{
		return $this->setParent($Input);
	}
	
	/**
	 * @deprecated use getParent()
	 */
	public function getParet()
	{
		return $this->getParent();
	}

		/**
	 * Set Parent of new child
	 * @param \marcoc\input\Input $Input
	 * @return \marcoc\input\Input
	 */
	protected function setParent( Input $Input )
	{
		$this->Parent = $Input;
		return $this;
	}
	
	/**
	 * Get Parent Input if exists
	 * @return \marcoc\input\Input
	 */
	public function getParent()
	{
		return $this->Parent;
	}
	
	private function initSharedStorage()
	{
		$this->SharedStorage = new \ArrayObject([],\ArrayObject::ARRAY_AS_PROPS);
		return $this;
	}
	
	/**
	 * 
	 * @param bool $local if false (default) return top parent SharedStorage
	 * @return \ArrayObject
	 */
	public function getSharedStorage( $local = false )
	{
		if( ! $this->Parent || $local ){
			return $this->SharedStorage;
		} else {
			return $this->Parent->getSharedStorage();
		}
	}
	
	public function isValid()
	{
		$is_valid = true;
		
		if( ! $this->Parent ){
			$this->shared_storage = [];
		}
		
		$this->initSharedStorage();
		
		$this->Errors->list = [];
		
		$process_as_array = $this->getProcessAsArray( $this->process_as_array );
		
		if( $process_as_array === null ){
			$is_valid = false;
			return false;
		}
		
		// Input own CHAIN
		
		if( $process_as_array ){
			
			if( $this->process_as_array_from_child_index > 0 ){
				if( ! $this->processItem() ){
					$is_valid = false;

					if( ! $this->continue_on_error ){
						goto end;
					}
				}
				// if after chain 
				if( $this->source_data === null && $this->stop_if_null ){
					$this->data = $this->source_data;
					goto end;
				}
				if( empty($this->source_data) && $this->stop_if_empty ){
					$this->data = $this->source_data;
					goto end;
				}
			}

			if( ! is_array($this->source_data) ){
				$this->error('vartype');
				$is_valid = false;
				return false;
			}

			foreach ( $this->source_data as $k => $item ){
				
				if( ! $this->processItem($k) ){
					$is_valid = false;
					
					if( ! $this->continue_on_error ){
						break;
					}
				}
			}
			
		} else {
			if( ! $this->processItem() ){
				$is_valid = false;
			}
		}
		
		if( ! $is_valid ){
			goto end;
		}
		
		// if after chain 
		if( $this->source_data === null && $this->stop_if_null ){
			$this->data = $this->source_data;
			goto end;
		}
		if( empty($this->source_data) && $this->stop_if_empty ){
			$this->data = $this->source_data;
			goto end;
		}
		
		
		$process_childs_as_array = $this->getProcessAsArray( $this->process_childs_as_array );
		
		if( $process_childs_as_array === null ){
			$is_valid = false;
			return false;
		}
		
		// CHILDS processing
		
		if( empty($this->childs) ){
			$this->data = $this->source_data;
			goto end;
		}
		
		if( $process_childs_as_array ){
			
			if( ! is_array($this->source_data) ){
				$this->error('vartype');
				$is_valid = false;
				return false;
			}

			foreach ( $this->source_data as $k => $item ){
				
				if( ! $this->processChildsOnItem($k) ){
					$is_valid = false;
					if( ! $this->continue_on_error ){
						goto end;
					}
				}
			}
			
		} else {
			
			if( ! $this->processChildsOnItem() ){
				$is_valid = false;
				goto end;
			}
		}
		
		end:

		return $is_valid;
	}

	
	/**
	 *
	 * @param mixed $item
	 * @param boolean $is_valid
	 * @return mixed
	 */
	private function processItem( $key = null )
	{
		$is_valid = true;
		
		$process_as_array = $this->getProcessAsArray( $this->process_as_array );
		$processing_as_an_array = isset($key);
		$this->array_index = $key;

		$target = $processing_as_an_array ? $this->source_data[$key] : $this->source_data;
		
		foreach( $this->process_chain as $index => &$process_item ){
			
			if( $process_as_array && $processing_as_an_array && $index < $this->process_as_array_from_child_index ){
				continue;
			}

			if( $process_as_array && ! $processing_as_an_array && $index >= $this->process_as_array_from_child_index ){
				continue;
			}

			if( $process_item instanceof Validator\ValidatorInterface ){
				
				if( ! $process_item->isValid( $target ) ){
					$this->error( $process_item->getError() );
					$is_valid = false;
					break;
				}
				
			} elseif ( $process_item instanceof Filter\FilterInterface ){
				
				if( $process_item instanceof Filter\FilterValidateInterface ){
					$_tmp = null;
					$target = $process_item->filter($target, $_tmp);
					if( ! $_tmp ){
						$this->error( $process_item->getError() );
						$is_valid = false;
						break;
					}
				} else {
					$target = $process_item->filter($target);
				}
				
			} elseif( $process_item instanceof ChainModifier ){
				
				$return_signal = call_user_func( $process_item->callback , $this , $target );
				
				if( $return_signal & ChainModifier::SIGNAL_ERROR ){
					$this->error( 'unknown' );
					$is_valid = false;
					break;
				}
				
				if( $return_signal & ChainModifier::SIGNAL_STOP_CHAIN ){
					break;
				}
				
			}
			
			if( $target === null && $this->stop_if_null ){		
				break;
			}
			if( empty($target) && $this->stop_if_empty ){
				break;
			}
			
		}
		
		if( isset($key) ){
			$this->source_data[$key] = $target;
		} else {
			$this->source_data = $target;
		}
		
		return $is_valid;
	}

	private function processChildsOnItem( $key = null )
	{
		$is_valid = true;
		
		$processing_as_an_array = isset($key);
		$this->array_index = $key;

		$target = $processing_as_an_array ? $this->source_data[$key] : $this->source_data;

		if( ! is_array($target) ){
			$this->error('vartype');
			$is_valid = false;
			goto end;
		}
		
		foreach( $this->childs as &$Child ){

			if( $this->validation_group && ! in_array($Child->name, $this->validation_group) ){
				continue;
			}
			
			// VALIDATE IF EXISTS
			$exists = array_key_exists($Child->name, $target);
			
			if( ! $exists ){
				
				if( $Child->required ){
					$this->error('missing',$Child->name);
					$is_valid = false;
					if( ! $this->continue_on_error ){
						goto end;
					}
				}
				
				if( $Child->hasUseDefaultIfNotExists() ){
					if( isset($key) ){
						$this->source_data[$key][$Child->name] = $this->data[$key][$Child->name] = $Child->getDefaultIfNotExists();
					} else {
						$this->source_data[$Child->name] = $this->data[$Child->name] = $Child->getDefaultIfNotExists();
					}
				}
				
				continue;
			}
			
			$Child->setData( $target[$Child->name] );
			
			$is_valid = $Child->isValid() ? $is_valid : false;
			
			$target[$Child->name] = $Child->getData();
			
			if( $target[$Child->name] === null && $Child->unset_if_null ){
				if( $key ){
					unset( $this->source_data[$key][$Child->name] );
				} else {
					unset( $this->source_data[$Child->name] );
				}
			} elseif( empty($target[$Child->name]) && $Child->unset_if_empty ){
				if( $key ){
					unset( $this->source_data[$key][$Child->name] );
				} else {
					unset( $this->source_data[$Child->name] );
				}
			} else {
				if( isset($key) ){
					$this->data[$key][$Child->name] = $target[$Child->name];
					$this->source_data[$key][$Child->name] = $Child->getFullData();
				} else {
					$this->data[$Child->name] = $target[$Child->name];
					$this->source_data[$Child->name] = $Child->getFullData();
				}
			}
			
			if( ! $is_valid && ! $this->continue_on_error ){
				goto end;
			}
		}
		
		end:
		return $is_valid;
	}
	
	private function getProcessAsArray( $process_mode )
	{
		$bool = false;
		
		if( $process_mode !== self::PROCESS_AS_ARRAY_NONE ){
			
			if( $process_mode === self::PROCESS_AS_ARRAY_ALWAYS ){
				
				$bool = true;
				
			} elseif( $process_mode === self::PROCESS_AS_ARRAY_IF_THEOR_ARRAY ){
				
				$bool = Validator\IsTheoreticArray::valid($this->source_data);
				
			} elseif( $process_mode === self::PROCESS_AS_ARRAY_IF_THEOR_OBJECT ){
				
				$bool = Validator\IsTheoreticObject::valid($this->source_data);
				
			} else { // $IF
				
				$bool = is_array($this->source_data);
			}
			
		}
		
		return $bool;
	}
	
	/**
	 * Register error in Input and parents
	 * @param string $code
	 * @param string $name
	 * @return \marcoc\input\Input
	 */
	public function error( string $code, $name = null )
	{
		$group[] = $this->name;
		if( isset($this->array_index) ){
			$group[] = (string) $this->array_index;
		}
		if( isset($name) ){
			$group[] = $name;
		}

		$name = implode('.' , $group);
		
		$this->Errors->add( new Error($name, $code) );
		
		if( $this->Parent ){
			$this->Parent->error($code, $name);
		}
		
		return $this;
	}

	/**
	 * 
	 * @param int $depth if > 1 (default), will set childrens recursively to the past depth.
	 * If -1 will set all recursively without depth limit.
	 * @return \marcoc\input\Input
	 */
	public function setAllNotRequired( int $depth = 1 )
	{
		if( $depth === 0 ){
			return $this;
		}
		
		foreach( $this->getChilds() as $Input ){
			$Input->required = false;
			if( $depth > 1 ){
				$Input->setAllNotRequired( $depth - 1 );
			} elseif( $depth === -1 ){
				$Input->setAllNotRequired( -1 );
			}
		}
		
		return $this;
	}
}

