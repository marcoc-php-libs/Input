<?php
namespace marcoc\input;

class ChainModifier
{
	CONST SIGNAL_ERROR = 1;
	CONST SIGNAL_STOP_CHAIN = 2;
	
	public $callback;
	
	public function __construct( $callback )
	{
		if( ! is_callable($callback) ){
			throw new \Error('first parameter must be callable');	
		}
		
		$this->callback = $callback;	
	}
	
}

