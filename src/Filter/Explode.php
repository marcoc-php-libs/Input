<?php
namespace marcoc\input\Filter;

class Explode implements FilterInterface
{
	private $delimiter;
	private $limit;
	
	public function __construct( string $delimiter , int $limit = null )
	{
		$this->setDelimiter($delimiter);
		if( $limit !== null ){
			$this->setLimit($limit);
		}
	}

	/**
	 * 
	 * @return string 
	 */
	public function getDelimiter()
	{
		return $this->delimiter;
	}

	/**
	 * 
	 * @param string $delimiter 
	 * @return \marcoc\input\Filter\Explode 
	 */
	public function setDelimiter( string $delimiter )
	{
		$this->delimiter = $delimiter;
		return $this;
	}

	/**
	 * 
	 * @return int 
	 */
	public function getLimit()
	{
		return $this->limit;
	}

	/**
	 * 
	 * @param int $limit 
	 * @return \marcoc\input\Filter\Explode 
	 */
	public function setLimit( int $limit )
	{
		$this->limit = $limit;
		return $this;
	}
	
	public function filter( $value )
	{
		if( ! is_string($value) ){
			return $value;
		}

		if( $this->limit !== null ){
			return explode($this->delimiter , $value , $this->limit);
		} else {
			return explode($this->delimiter , $value);
		}
	}
}
