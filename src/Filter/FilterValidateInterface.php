<?php
namespace marcoc\input\Filter;

interface FilterValidateInterface
{
	/**
	 * Run filter and return altered value, with validation process
	 * @param mixed $value
	 * @param boolean $is_valid is set with true or false
	 */
	public function filter($value , &$is_valid = null);
	
	/**
	 * @return string error code as string
	 */
	public function getError();
}

