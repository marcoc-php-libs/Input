<?php
namespace marcoc\input\Filter;

class Cast implements FilterInterface
{

	CONST INT = 'INT';
	CONST FLOAT = 'FLOAT';
	CONST STRING = 'STRING';
	CONST BOOLEAN = 'BOOLEAN';
	CONST ARRAY = 'ARRAY';
	CONST OBJECT = 'OBJECT';
	
	private $to;
	
	public function __construct( $to )
	{
		$this->to = $to;
	}
	
	public function filter( $value )
	{
		if( $this->to === self::INT ){
			return (int) $value;
		} elseif( $this->to === self::FLOAT ){
			return (float) $value;
		} elseif( $this->to === self::STRING ){
			return (string) $value;
		} elseif( $this->to === self::BOOLEAN ){
			return (bool) $value;
		} elseif( $this->to === self::ARRAY ){
			return (array) $value;
		} elseif( $this->to === self::OBJECT ){
			return (object) $value;
		}
	}
}

