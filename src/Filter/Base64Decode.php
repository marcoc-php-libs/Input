<?php
namespace marcoc\input\Filter;

class Base64Decode implements FilterValidateInterface , FilterInterface
{
	private $strict = false;
	
	private $error;
	
	public function __construct( $strict = false )
	{
		$this->strict = $strict;
	}
	
	public function filter( $value , &$is_valid = false )
	{
		$value = base64_decode( $value , $this->strict );
		
		$is_valid = $value !== false;
		
		if( ! $is_valid ){
			$this->error = 'invalid_base64';
		}
		
		return $value;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

