<?php
namespace marcoc\input\Filter;

class Trim implements FilterInterface
{
	CONST DIRECTION_R = 'R';
	CONST DIRECTION_L = 'L';
	
	private $chars;
	private $direction;
	
	public function __construct( string $chars = null , $direction = null )
	{
		$this->chars = $chars;
		$this->direction = $direction;
	}
	
	public function filter( $value )
	{
		if( ! is_string($value) ){
			return $value;
		}
		
		$function = strtolower( $this->direction.'trim' );
		
		if( $this->chars ){
			return $function($value,$this->chars);
		} else {
			return $function($value);
		}
	}
}

