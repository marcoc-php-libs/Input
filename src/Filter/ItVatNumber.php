<?php
namespace marcoc\input\Filter;

/**
 * 
 * @link http://www.icosaedro.it/cf-pi/ thank for code
 *
 */
class ItVatNumber extends AbstractFilter implements FilterValidateInterface , FilterInterface
{
	CONST ATTR_ALLOW_COUNTRY_PREFIX = 'allow_country_prefix';
	CONST ATTR_RETURN_MODE = 'return_mode';
	
	CONST RETURN_MODE_NATIONAL = 'national';
	CONST RETURN_MODE_INTERNATIONAL = 'international';
	
	protected $allow_country_prefix = true;
	protected $return_mode = self::RETURN_MODE_INTERNATIONAL;
	
	private $error;
	
	/**
	 * 
	 * @param array $options
	 */
	public function __construct( array $options = [] )
	{
		$this->allow_country_prefix = $options['allow_country_prefix'] ?? $this->allow_country_prefix;
		$this->return_mode = $options['return_mode'] ?? $this->return_mode;
	}
	
	public function filter( $value , &$is_valid = null )
	{
		$is_valid = true;
		
		if( strlen($value) === 13 && $value[0] === 'I' && $value[1] === 'T' ){
			if( ! $this->allow_country_prefix ){
				$this->error = 'not valid';
				$is_valid = false;
				return $value;
			}
			$value = substr($value, 2);
		}
		
		if( strlen($value) !== 11 || ! ctype_digit($value) ){
			$this->error = 'not valid';
			$is_valid = false;
			return $value;
		}
		$s = 0;
		
		for ($i = 0; $i <= 9; $i += 2) {
			$s += ord($value[$i]) - ord('0');
		}
		for ($i = 1; $i <= 9; $i += 2) {
			$c = 2 * (ord($value[$i]) - ord('0'));
			if ($c > 9){
				$c = $c - 9;
			}
			$s += $c;
		}
		if ((10 - $s % 10) % 10 != ord($value[10]) - ord('0')) {
			$this->error = 'not valid';
			$is_valid = false;
			return $value;
		}
		
		if( $this->return_mode === self::RETURN_MODE_NATIONAL ){
			
		} else {
			$value = "IT$value";
		}
		
		return $value;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

