<?php
namespace marcoc\input\Filter;

class JsonEncode implements FilterValidateInterface , FilterInterface
{
	private $error;
	
	private $max_depth = null;
	private $json_options = null;
	
	public function __construct( int $max_depth = 512, int $json_options = 0 )
	{
		$this->max_depth = $max_depth;
		$this->json_options = $json_options;
	}
	
	public function filter( $value , &$is_valid = null )
	{
		try {
			
			$value = json_encode( $value , $this->json_options , $this->max_depth );
			$is_valid = ! json_last_error();
			
		} catch (\Exception $e) {
			$is_valid = false;
		}
		
		if( ! $is_valid ){
			$this->error = 'json_encode';
		}
		
		return $value;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

