<?php
namespace marcoc\input\Filter;

use marcoc\input\Filter\FilterInterface;

class Callback implements FilterInterface
{
	
	private $Callback;
	
	/**
	 * @param callable $Callback
	 */
	public function __construct( callable $Callback )
	{
		$this->Callback = $Callback;
	}
	
	public function filter( $value )
	{
		$Callback = $this->Callback;
		return $Callback( $value , $this );
	}
}

