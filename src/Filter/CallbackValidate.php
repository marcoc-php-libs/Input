<?php
namespace marcoc\input\Filter;

class CallbackValidate implements FilterValidateInterface , FilterInterface
{
	
	private $Callback;
	
	private $error;
	
	/**
	 * @param callable $Callback
	 */
	public function __construct( callable $Callback )
	{
		$this->Callback = $Callback;
	}
	
	public function filter( $value , &$is_valid = null )
	{
		$Callback = $this->Callback;
		return $Callback( $value , $is_valid , $this );
	}
	
	public function setError( string $error )
	{
		$this->error = $error;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

