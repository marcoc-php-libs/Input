<?php
namespace marcoc\input\Filter;

class JsonDecode extends AbstractFilter implements FilterValidateInterface , FilterInterface
{
	CONST ATTR_DECODE_AS_OBJECT = 'decode_as_object';
	CONST ATTR_MAX_DEPTH = 'max_depth';
	CONST ATTR_JSON_OPTIONS = 'json_options';
	CONST ATTR_ALLOW_JUST_DECODED = 'allow_just_decoded';
	
	protected $decode_as_object = null;
	protected $max_depth = null;
	protected $json_options = null;
	protected $allow_just_decoded = false;
	
	private $error;
	
	public function __construct( $decode_as_object = false, int $max_depth = 512, int $json_options = 0 , array $options = [])
	{
		$this->decode_as_object = $decode_as_object;
		$this->max_depth = $max_depth;
		$this->json_options = $json_options;
		$this->allow_just_decoded = $options['allow_just_decoded'] ?? $this->allow_just_decoded;
	}
	
	public function filter( $value , &$is_valid = null )
	{
		if( ! is_string($value) ){
			$is_valid = $this->allow_just_decoded;
			goto end;
		}
		
		try {
			
			$value = json_decode( $value , ! $this->decode_as_object , $this->max_depth , $this->json_options );
			$is_valid = ! json_last_error();
			
		} catch (\Exception $e) {
			$is_valid = false;
		}
		
		end:
		
		if( ! $is_valid ){
			$this->error = 'invalid_json';
		}
		
		return $value;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

