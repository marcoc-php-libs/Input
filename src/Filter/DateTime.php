<?php
namespace marcoc\input\Filter;

class DateTime extends AbstractFilter implements FilterValidateInterface , FilterInterface
{

	protected $formats = [];
	protected $timezone;
	protected $immutable = false;
	
	private $error;
	
	public function __construct( array $formats , \DateTimeZone $timezone = null , bool $immutable = false )
	{
		$this->formats = $formats;
		$this->timezone = $timezone;
		$this->immutable = $immutable;
	}

	function setFormats( array $formats ) {
		$this->formats = $formats;
		return $this;
	}
	function getFormats() {
		return $this->formats;
	}

	function setTimezone( \DateTimeZone $timezone = null ) {
		$this->timezone = $timezone;
		return $this;
	}
	function getTimezone() {
		return $this->timezone;
	}

	
	public function filter( $value , &$is_valid = null )
	{
		$is_valid = false;

		if( ! is_string($value) ){
			goto end;
		}

		$class = $this->immutable ? \DateTimeImmutable::class : \DateTime::class;
		
		try {
			
			foreach( $this->formats as $format){
				$DT = $class::createFromFormat($format,$value,$this->timezone);

				$last_errors = $class::getLastErrors();

				if( ! $DT instanceof \DateTimeInterface ){
					continue;
				} elseif( $last_errors && ($last_errors['warning_count']+$last_errors['error_count']) > 0 ){
					continue;
				} else {
					$is_valid = true;
					$value = $DT;
					goto end;
				}
			}
			
		} catch (\Exception $e) {
			$is_valid = false;
		}
		
		end:
		
		if( ! $is_valid ){
			$this->error = 'invalid_datetime';
		}
		
		return $value;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

