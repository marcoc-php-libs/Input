<?php
namespace marcoc\input\Filter;

interface FilterInterface
{
	/**
	 * Run filter and return altered value
	 * @param mixed $value
	 */
	public function filter($value);
}

