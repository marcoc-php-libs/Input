<?php
namespace marcoc\input\Filter;

use libphonenumber\PhoneNumberFormat;
use libphonenumber\PhoneNumberType;

class PhoneNumber extends AbstractFilter implements FilterValidateInterface , FilterInterface
{
	CONST ATTR_DEFAULT_COUNTRY = 'default_country';
	CONST ATTR_ALLOW_POSSIBLE = 'allow_possible';
	CONST ATTR_ALLOW_TYPES = 'allow_types';
	CONST ATTR_ALLOW_COUNTRIES = 'allow_countries';
	CONST ATTR_RETURN_TYPE = 'return_type';
	
	/**
	 * Listed for completeness.
	 * Not really supported the parsing of this number
	 */
	CONST TYPE_EMERGENCY = 1;
	CONST TYPE_FIXED_LINE = 2;
	CONST TYPE_FIXED_LINE_OR_MOBILE = 4;
	CONST TYPE_MOBILE = 8;
	CONST TYPE_PAGER = 16;
	CONST TYPE_PERSONAL_NUMBER = 32;
	CONST TYPE_PREMIUM_RATE = 64;
	CONST TYPE_SHARED_COST = 128;
	CONST TYPE_SHORT_CODE = 256;
	CONST TYPE_STANDARD_RATE = 512;
	CONST TYPE_TOLL_FREE = 1024;
	CONST TYPE_UAN = 2048;
	CONST TYPE_UNKNOWN = 4096;
	CONST TYPE_VOICEMAIL = 8192;
	CONST TYPE_VOIP = 16384;
	
	CONST RETURN_UNMODIFIED = 'unmodified';
	CONST RETURN_E164 = 'E164';
	CONST RETURN_INTERNATIONAL = 'INTERNATIONAL';
	CONST RETURN_NATIONAL = 'NATIONAL';
	CONST RETURN_RFC3966 = 'RFC3966';
	CONST RETURN_OBJECT = 'OBJECT';
	
	protected $default_country = null;
	protected $allow_possible = false;
	protected $allow_types = self::TYPE_FIXED_LINE|self::TYPE_FIXED_LINE_OR_MOBILE|self::TYPE_MOBILE;
	protected $allow_countries = [];
	protected $return_type = self::RETURN_E164;
	
	private $error;
	
	/**
	 * @param array $options
	 */
	public function __construct( array $options = [] )
	{
		$this->default_country = $options['default_country'] ?? $this->default_country;
		
		if( isset($options[self::ATTR_DEFAULT_COUNTRY]) ){
			$this->default_country = strtoupper($options[self::ATTR_DEFAULT_COUNTRY]);
		}
		
		$this->allow_possible = $options['allow_possible'] ?? $this->allow_possible;
		$this->allow_types = $options['allow_types'] ?? $this->allow_types;
		if( isset($options[self::ATTR_ALLOW_COUNTRIES]) ){
			$this->allow_countries = array_map(function($v){
				return strtoupper($v);
			}, $options[self::ATTR_ALLOW_COUNTRIES]);
		}
		$this->return_type = $options['return_type'] ?? $this->return_type;
	}
	
	public function filter( $value , &$is_valid = null )
	{
		$is_valid = true;
		$phoneUtil = \libphonenumber\PhoneNumberUtil::getInstance();
		$shortNumberInfo = \libphonenumber\ShortNumberInfo::getInstance();
		
		if( empty($value) || ! is_string($value) ){
			$this->error = 'not parsable';
			$is_valid = false;
			return $value;
		}
		
		try {
			$phoneNumber = $phoneUtil->parse( $value , $this->default_country );
		} catch (\Exception $e) {
			if( $value[0] !== '+' ){
				try {
					$phoneNumber = $phoneUtil->parse( "+$value" , $this->default_country );
				} catch (\Exception $e) {}
			}
		}
		
		if( empty($phoneNumber) ){
			$this->error = 'not parsable';
			$is_valid = false;
			return $value;
		}
		
		if( ! $phoneUtil->isValidNumber($phoneNumber) && ! $shortNumberInfo->isValidShortNumber($phoneNumber) ){
			if( ! $this->allow_possible || (! $phoneUtil->isPossibleNumber($phoneNumber) && ! $shortNumberInfo->isPossibleShortNumber($phoneNumber)) ){
				$this->error = 'not valid';
				$is_valid = false;
				return $value;
			}
		}
		
		$type = $phoneUtil->getNumberType($phoneNumber);
		$number_types_map = $this->getNumberTypesMap();
		
		$type = $number_types_map[$type] ?? 0;
		
		if( ! ($type & $this->allow_types) ){
			$this->error = 'type not valid';
			$is_valid = false;
			return $value;
		}
		
		$country_code = $phoneUtil->getRegionCodeForNumber($phoneNumber);
		$country_code_lower = strtolower($country_code);
		
		if( $this->allow_countries ){
			if( ! in_array($country_code, $this->allow_countries) && ! in_array($country_code_lower, $this->allow_countries) ){
				$this->error = 'country not valid';
				$is_valid = false;
				return $value;
			}
		}
		
		if( $this->return_type === self::RETURN_E164 ){
			$value = $phoneUtil->format($phoneNumber, PhoneNumberFormat::E164);
		} elseif( $this->return_type === self::RETURN_INTERNATIONAL ){
			$value = $phoneUtil->format($phoneNumber, PhoneNumberFormat::INTERNATIONAL);
		} elseif( $this->return_type === self::RETURN_NATIONAL ){
			$value = $phoneUtil->format($phoneNumber, PhoneNumberFormat::NATIONAL);
		} elseif( $this->return_type === self::RETURN_OBJECT ){
			$value = $phoneNumber;
		} elseif( $this->return_type === self::RETURN_RFC3966 ){
			$value = $phoneUtil->format($phoneNumber, PhoneNumberFormat::RFC3966);
		} elseif( $this->return_type === self::RETURN_UNMODIFIED ){
			
		}
		
		return $value;
	}
	
	public function getError()
	{
		return $this->error;
	}
	
	private function getNumberTypesMap()
	{
		return [
			PhoneNumberType::EMERGENCY => self::TYPE_EMERGENCY,
			PhoneNumberType::FIXED_LINE => self::TYPE_FIXED_LINE,
			PhoneNumberType::FIXED_LINE_OR_MOBILE => self::TYPE_FIXED_LINE_OR_MOBILE,
			PhoneNumberType::MOBILE => self::TYPE_MOBILE,
			PhoneNumberType::PAGER => self::TYPE_PAGER,
			PhoneNumberType::PERSONAL_NUMBER => self::TYPE_PERSONAL_NUMBER,
			PhoneNumberType::PREMIUM_RATE => self::TYPE_PREMIUM_RATE,
			PhoneNumberType::SHARED_COST => self::TYPE_SHARED_COST,
			PhoneNumberType::SHORT_CODE => self::TYPE_SHORT_CODE,
			PhoneNumberType::STANDARD_RATE => self::TYPE_STANDARD_RATE,
			PhoneNumberType::TOLL_FREE => self::TYPE_TOLL_FREE,
			PhoneNumberType::UAN => self::TYPE_UAN,
			PhoneNumberType::UNKNOWN => self::TYPE_UNKNOWN,
			PhoneNumberType::VOICEMAIL => self::TYPE_VOICEMAIL,
			PhoneNumberType::VOIP => self::TYPE_VOIP,
		];
	}
}

