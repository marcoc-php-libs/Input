<?php
namespace marcoc\input\Filter;

class FileUpload extends AbstractFilter implements FilterValidateInterface , FilterInterface
{
	CONST ATTR_SET_NULL_IF_NOT_SEND = 'set_null_if_not_send';
	
	CONST ATTR_ALLOW_FILE_UPLOAD = 'allow_file_upload';
	CONST ATTR_ALLOW_BASE64 = 'allow_base64';
	
	protected $set_null_if_not_send = true;
	
	protected $allow_file_upload = true;
	protected $allow_base64 = false;
	
	private $error;
	
	public function __construct( array $options = [] )
	{
		$this->set_null_if_not_send = $options['set_null_if_not_send'] ?? $this->set_null_if_not_send;
		$this->allow_file_upload = $options['allow_file_upload'] ?? $this->allow_file_upload;
		$this->allow_base64 = $options['allow_base64'] ?? $this->allow_base64;
	}
	
	public function filter( $value , &$is_valid = null )
	{
		$is_valid = true;
		
		if( empty($value) ){
			
			if( $this->set_null_if_not_send ){
				return null;
			} else {
				$is_valid = false;
				$this->error = 'not uploaded';
				return $value;
			}
			
		} elseif( is_string($value) ){ // BASE64
			
			if( ! $this->allow_base64 ){
				$is_valid = false;
				$this->error = 'vartype';
				return $value;
			}
			
			$value = base64_decode($value,true);
			if( ! $value ){
				$is_valid = false;
				$this->error = 'format';
				return $value;
			}
			
			$dir = tempnam(sys_get_temp_dir(), 'INPUT_FileUpload_');
			if( ! $dir ){
				throw new \Exception("Impossible create temp file");
			}
			file_put_contents($dir, $value);
			return $dir;
			
		} elseif( is_array($value) ){ // MULTIPART UPLOAD
			
			if( ! $this->allow_file_upload ){
				$is_valid = false;
				$this->error = 'vartype';
				return $value;
			}
			
			if( ! is_uploaded_file($value['tmp_name']??'') ){
				$is_valid = false;
				$this->error = 'format';
				return $value;
			}
			return $value['tmp_name'];
			
		} else {
			$is_valid = false;
			$this->error = 'vartype';
			return $value;
		}
		
	}
	
	public function getError()
	{
		return $this->error;
	}
}

