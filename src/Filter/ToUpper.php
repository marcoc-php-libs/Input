<?php
namespace marcoc\input\Filter;

class ToUpper implements FilterInterface
{
	public function filter( $value )
	{
		return strtoupper($value);
	}
}

