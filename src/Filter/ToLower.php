<?php
namespace marcoc\input\Filter;

class ToLower implements FilterInterface
{
	public function filter( $value )
	{
		return strtolower($value);
	}
}

