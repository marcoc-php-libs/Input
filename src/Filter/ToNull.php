<?php
namespace marcoc\input\Filter;

class ToNull implements FilterInterface
{
	
	CONST EMPTY = 'EMPTY';
	CONST STRING_EMPTY = 'STRING_EMPTY';
	CONST STRING_ZERO = 'STRING_ZERO';
	CONST ZERO_INT = 'ZERO_INT';
	CONST ZERO_FLOAT = 'ZERO_FLOAT';
	CONST FALSE = 'FALSE';
	CONST EMPTY_ARRAY = 'EMPTY_ARRAY';
	
	private $if;
	
	public function __construct( array $if = [ self::EMPTY ] )
	{
		$this->if = $if;
	}
	
	public function filter( $value )
	{
		foreach( $this->if as $if ){
			
			if( $if === self::EMPTY ){
				if( empty($value) ){
					$value = null;
					break;
				}
			} elseif( $if === self::STRING_EMPTY ){
				if( $value === '' ){
					$value = null;
					break;
				}
			} elseif( $if === self::STRING_ZERO ){
				if( $value === '0' ){
					$value = null;
					break;
				}
			} elseif( $if === self::ZERO_INT ){
				if( $value === 0 ){
					$value = null;
					break;
				}
			} elseif( $if === self::ZERO_FLOAT ){
				if( $value === 0.0 ){
					$value = null;
					break;
				}
			} elseif( $if === self::FALSE ){
				if( $value === false ){
					$value = null;
					break;
				}
			} elseif( $if === self::EMPTY_ARRAY ){
				if( $value === [] ){
					$value = null;
					break;
				}
			}
		}
		
		return $value;
	}
}

