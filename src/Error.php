<?php
namespace marcoc\input;

class Error
{
	
	public $name;
	public $code;
	
	public function __construct( string $name , string $code )
	{
		$this->name = $name;
		$this->code = $code;
	}
	
	public function __toString()
	{
		return $this->name . ' ' . $this->code;
	}
}

