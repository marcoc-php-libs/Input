<?php
namespace marcoc\input\Validator;

class Not implements ValidatorInterface
{
	
	private $Validator;
	
	private $error;
	
	/**
	 * 
	 * @param ValidatorInterface $Validator
	 */
	public function __construct( ValidatorInterface $Validator )
	{
		$this->Validator = $Validator;
	}
	
	public function isValid( $value )
	{
		if( $this->Validator->isValid($value) ){
			$this->error = 'not';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

