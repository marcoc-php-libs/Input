<?php
namespace marcoc\input\Validator;

class Equal implements ValidatorInterface
{
	private $to;
	private $strict = false;
	
	private $error;
	
	/**
	 * 
	 * @param mixed $to compared with
	 * @param string $strict use === instead ==
	 */
	public function __construct( $to , $strict = false )
	{
		$this->to = $to;
		$this->strict = $strict;
	}
	
	public function isValid( $value )
	{
		if( $this->strict ){
			$bool = $value === $this->to;
		} else {
			$bool = $value == $this->to;
		}
		
		if( ! $bool ){
			$this->error = 'not equal';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

