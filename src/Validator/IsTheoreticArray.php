<?php
namespace marcoc\input\Validator;

class IsTheoreticArray implements ValidatorInterface
{
	private $error;
	
	public function isValid( $value )
	{
		if( ! self::valid($value) ){
			$this->error = 'vartype';
			return false;
		}
		
		return true;
	}
	
	public static function valid( $value )
	{
		if( ! is_array($value) ){
			return false;
		}
		
		$i = 0;
		foreach ($value as $k => $v){
			if( $k !== $i++ ){
				return false;
			}
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

