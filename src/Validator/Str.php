<?php
namespace marcoc\input\Validator;

class Str implements ValidatorInterface
{
	private $min;
	private $max;
	private $eq;
	private $encoding;
	
	private $error;
	
	/**
	 * 
	 * @param string $min_length
	 * @param string $max_length
	 * @param string $eq_length
	 * @param string $encoding
	 */
	public function __construct( $min= null , $max = null , $eq = null , $encoding = null )
	{
		$this->min = $min;
		$this->max = $max;
		$this->eq = $eq;
		$this->encoding = $encoding;
	}
	
	public function isValid( $value )
	{
		if( $this->encoding ){
			$length = mb_strlen( $value , $this->encoding );
		} else {
			$length = strlen( $value );
		}
		
		$valid = true;
		
		if( isset($this->min) && $length < $this->min ){
			$valid = false;
		} elseif( isset($this->max) && $length > $this->max ){
			$valid = false;
		} elseif( isset($this->eq) && $length !== $this->eq ){
			$valid = false;
		}
		
		if( ! $valid ){
			$this->error = 'string_length';
		}
		
		return $valid;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

