<?php
namespace marcoc\input\Validator;

class ArrayIsList implements ValidatorInterface
{
	private $error;
	
	public function isValid( $value )
	{
		$is_valid = true;

		if( ! is_array($value) ){
			$is_valid = false;
			goto end;
		}

		if( function_exists('array_is_list') ){
			if( ! array_is_list($value) ){
				$is_valid = false;
				goto end;
			}
		} else {
			$i = 0;
			foreach ($value as $k => $v){
				if( $k !== $i++ ){
					$is_valid = false;
					goto end;
				}
			}
		}


		end:

		if( ! $is_valid ){
			$this->error = 'vartype';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

