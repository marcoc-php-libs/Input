<?php
namespace marcoc\input\Validator;

class Between implements ValidatorInterface
{
	private $min;
	private $max;
	
	private $error;
	
	/**
	 *
	 * @param string $min
	 * @param string $max
	 */
	public function __construct( $min = null , $max = null )
	{
		$this->min = $min;
		$this->max = $max;
	}
	
	public function isValid( $value )
	{
		if( isset($this->min) && $value < $this->min ){
			$this->error = 'value_lower';
			return false;
		} elseif( isset($this->max) && $value > $this->max ){
			$this->error = 'value_greater';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

