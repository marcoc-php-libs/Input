<?php
namespace marcoc\input\Validator;

use marcoc\input\PCREString;

/**
 * @property array[string]|marcoc\input\PCREString $allowed_scheme
 * @property bool|array[string]|marcoc\input\PCREString $allowed_user
 * @property bool|array[string]|marcoc\input\PCREString $allowed_pass
 * @property array[string]|marcoc\input\PCREString $allowed_host
 * @property bool|array[string]|marcoc\input\PCREString $allowed_port
 * @property bool|array[string]|marcoc\input\PCREString $allowed_path
 * @property bool|array[string]|marcoc\input\PCREString $allowed_query
 * @property bool|array[string]|marcoc\input\PCREString $allowed_fragment 
 * @property array $check_hostname_options see $allowed_scheme
 */

class Url implements ValidatorInterface
{
	
	CONST ATTR_ALLOWED_SCHEME = 'allowed_scheme';
	CONST ATTR_ALLOWED_USER = 'allowed_user';
	CONST ATTR_ALLOWED_PASS = 'allowed_pass';
	CONST ATTR_ALLOWED_HOST = 'allowed_host';
	CONST ATTR_ALLOWED_PORT = 'allowed_port';
	CONST ATTR_ALLOWED_PATH = 'allowed_path';
	CONST ATTR_ALLOWED_QUERY = 'allowed_query';
	CONST ATTR_ALLOWED_FRAGMENT = 'allowed_fragment';
	
	CONST ATTR_CHECK_HOSTNAME_OPTIONS = 'check_hostname_options';
	
	
	private $allowed_scheme = true;
	private $allowed_user = null;
	private $allowed_pass = null;
	private $allowed_host = true;
	private $allowed_port = null;
	private $allowed_path = null;
	private $allowed_query = null;
	private $allowed_fragment = null;
	
	private $check_hostname_options = [];
	
	private $error;
	
	
	public function __construct( array $options = [] )
	{
		$this->allowed_scheme = $options['allowed_scheme'] ?? $this->allowed_scheme;
		$this->allowed_user = $options['allowed_user'] ?? $this->allowed_user;
		$this->allowed_pass = $options['allowed_pass'] ?? $this->allowed_pass;
		$this->allowed_host = $options['allowed_host'] ?? $this->allowed_host;
		$this->allowed_port = $options['allowed_port'] ?? $this->allowed_port;
		$this->allowed_path = $options['allowed_path'] ?? $this->allowed_path;
		$this->allowed_query = $options['allowed_query'] ?? $this->allowed_query;
		$this->allowed_fragment = $options['allowed_fragment'] ?? $this->allowed_fragment;
		$this->check_hostname_options = $options['check_hostname_options'] ?? $this->check_hostname_options;
	}
	
	public function __set( $name , $value )
	{
		if( substr($name, 0, 7) === 'allowed' ){
			if( !is_bool($value) && !is_array($value) && !$value instanceof PCREString ){
				throw new \Error("Allowed value must be bool, array or PCREString");
			}
			$this->{$name} = $value;
		} else {
			throw new \Error("You can't set $name");
		}
	}
	
	public function __get( $name )
	{
		if( substr($name, 0, 7) === 'allowed' ){
			return $this->{$name} ?? null;
		} else {
			throw new \Error("You can't get $name");
		}
	}
	
	public function isValid( $value )
	{
		if( ! $this->allowed_host || ! $this->allowed_scheme ){
			throw new \Exception("allowed_host & allowed_scheme are always required");
		}
		
		if( ! is_string($value) ){
			$this->error = 'vartype';
			return false;
		}
		
		if( ! filter_var($value , FILTER_VALIDATE_URL ) ){
			$this->error = 'not_url';
			return false;
		}
		
		$components = parse_url( $value );
		
		if( ! $components ){
			$this->error = 'not_url';
			return false;
		}
		
		foreach(['scheme','user','pass','host','port','path','query','fragment'] as $component){
			if( isset($this->{'allowed_'.$component}) ){
				$rule = $this->{'allowed_'.$component};
				
				if( $rule === true && empty($components[$component]) ){
					$this->error = "url_component_{$component}_required";
					return false;
				}
				
				if( $rule === false && isset($components[$component]) ){
					$this->error = "url_component_{$component}_not_allowed";
					return false;
				}
				
				if( is_array($rule) && ! in_array($components[$component]??null, $rule,true)){
					$this->error = 'not_allowed_'.$component;
					return false;
				}
			
				if( $rule instanceof PCREString && ! preg_match($rule->pattern, $components[$component])){
					$this->error = 'not_allowed_'.$component;
					return false;
				}
			}
		}
		
		if( $this->check_hostname_options && isset($components['host']) ){
			$Validator = new Hostname( $this->check_hostname_options );
			if( ! $Validator->isValid($components['host']) ){
				$this->error = $Validator->getError();
				return false;
			}
		}
		
		return true;
	}
	
	public static function valid( $value , array $options = [] )
	{
		$object = new self($options);
		return $object->isValid($value);
	}
	
	public function getError()
	{
		return $this->error;
	}
}

