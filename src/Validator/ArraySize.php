<?php
namespace marcoc\input\Validator;

class ArraySize implements ValidatorInterface
{
	private $min;
	private $max;
	private $eq;
	
	private $error;
	
	/**
	 * @param int $min
	 * @param int $max
	 * @param int $eq
	 */
	public function __construct( int $min = null , int $max = null , int $eq = null )
	{
		$this->min = $min;
		$this->max = $max;
		$this->eq = $eq;
	}

	/**
	 * 
	 * @return int|null 
	 */
	public function getMin()
	{
		return $this->min;
	}
	/**
	 * 
	 * @param int|null $min 
	 * @return \marcoc\input\Validator\ArraySize 
	 */
	public function setMin( int $min = null )
	{
		$this->min = $min;
		return $this;
	}

	/**
	 * 
	 * @return int|null 
	 */
	public function getMax()
	{
		return $this->max;
	}
	/**
	 * 
	 * @param int|null $max 
	 * @return \marcoc\input\Validator\ArraySize 
	 */
	public function setMax( int $max = null )
	{
		$this->max = $max;
		return $this;
	}

	/**
	 * 
	 * @return int|null 
	 */
	public function getEq()
	{
		return $this->eq;
	}
	/**
	 * 
	 * @param int|null $eq 
	 * @return \marcoc\input\Validator\ArraySize 
	 */
	public function setEq( int $eq = null )
	{
		$this->eq = $eq;
		return $this;
	}

	
	public function isValid( $value )
	{
		if( ! is_array($value) ){
			$this->error = 'not_array';
			return false;
		}

		$size = count($value);
		
		$valid = true;
		
		if( isset($this->min) && $size < $this->min ){
			$valid = false;
		} elseif( isset($this->max) && $size > $this->max ){
			$valid = false;
		} elseif( isset($this->eq) && $size !== $this->eq ){
			$valid = false;
		}
		
		if( ! $valid ){
			$this->error = 'array_size';
		}
		
		return $valid;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

