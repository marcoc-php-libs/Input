<?php
namespace marcoc\input\Validator;

class IsTheoreticObject implements ValidatorInterface
{
	protected $error;
	
	public function isValid( $value )
	{
		if( ! self::valid($value) ){
			$this->error = 'vartype';
			return false;
		}
		
		return true;
	}
	
	public static function valid( $value )
	{
		if( ! is_array($value) ){
			return false;
		}
		
		if( ! $value ){
			return true;
		}
		
		$i = 0;
		foreach ($value as $k => $v){
			if( $k !== $i++ ){
				return true;
			}
		}
		
		return false;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

