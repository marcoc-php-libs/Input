<?php
namespace marcoc\input\Validator;

class Pathinfo implements ValidatorInterface
{
	private $pathinfo_type;
	private $haystack;
	private $strict = false;
	
	private $error;
	
	/**
	 * @param int $pathinfo_type
	 * @param array $haystack array where in_array() run search
	 * @param string $strict strict search setted in in_array( array , search )
	 */
	public function __construct( $pathinfo , array $haystack = [] , $strict = false )
	{
		$this->pathinfo_type = $pathinfo;
		$this->haystack = $haystack;
		$this->strict = $this->strict;
	}
	
	public function isValid( $value )
	{
		$pathinfo = pathinfo($value , $this->pathinfo_type);
		
		if( ! $pathinfo ){
			$this->error = 'file_name';
			return false;
		}
		
		if( $this->haystack ){
			if( ! in_array($pathinfo, $this->haystack , $this->strict) ){
				$this->error = 'file_name';
				return false;
			}
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

