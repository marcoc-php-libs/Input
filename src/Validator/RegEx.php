<?php
namespace marcoc\input\Validator;

class RegEx implements ValidatorInterface
{
	private $pattern;
	
	private $error;
	
	/**
	 * 
	 * @param string $pattern
	 */
	public function __construct( string $pattern )
	{
		$this->pattern = $pattern;
	}
	
	public function isValid( $value )
	{
		if( ! preg_match($this->pattern, $value) ){
			$this->error = 'string_format';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

