<?php
namespace marcoc\input\Validator;

/**
 * 
 * @link http://www.icosaedro.it/cf-pi/ thank for code
 *
 */
class ItTaxCode extends AbstractValidator implements ValidatorInterface
{
	CONST ATTR_ALLOW_LEGAL_ENTITIES = 'allow_legal_entities';
	CONST ATTR_ALLOW_RECOGNIZED_ASSOCIATIONS = 'allow_recognized_associations';
	CONST ATTR_ALLOW_UNRECOGNIZED_ASSOCIATIONS = 'allow_unrecognized_associations';
	
	protected $allow_legal_entities = true;
	protected $allow_recognized_associations = true;
	protected $allow_unrecognized_associations = true;
	
	private $error;
	
	/**
	 * 
	 * @param array $options
	 */
	public function __construct( array $options = [] )
	{
		$this->allow_legal_entities = $options['allow_legal_entities'] ?? $this->allow_legal_entities;
		$this->allow_recognized_associations = $options['allow_recognized_associations'] ?? $this->allow_recognized_associations;
		$this->allow_unrecognized_associations = $options['allow_unrecognized_associations'] ?? $this->allow_unrecognized_associations;
	}
	
	public function isValid( $value )
	{
		if( ctype_digit($value) ){
			
			$len = strlen($value);
			
			if( ($len === 11 && $this->allow_legal_entities) ||
				($len === 8 && $this->allow_recognized_associations ) ||
				($len == 9 && $this->allow_unrecognized_associations) ){
				// ok
			} else {
				$this->error = 'not valid';
				return false;
			}
			
		} else {
			
			$regex = '[A-Z]{6}'.
				'[0-9LMNPQRSTUV]{2}'.
				'[ABCDEHLMPRST]{1}'.
				'[0-9LMNPQRSTUV]{2}'.
				'[A-Z]{1}'.
				'[0-9LMNPQRSTUV]{3}'.
				'[A-Z]{1}';
			
			if( ! preg_match('/^'.$regex.'$/D', $value) ){
				$this->error = 'not valid';
				return false;
			}
			
			$cf = $value;
			
			$s = 0;
			for( $i = 1; $i <= 13; $i += 2 ){
				$c = $cf[$i];
				if( strcmp($c, "0") >= 0 && strcmp($c, "9") <= 0 ){
					$s += ord($c) - ord('0');
				} else {
					$s += ord($c) - ord('A');
				}
			}
			
			for( $i = 0; $i <= 14; $i += 2 ){
				$c = $cf[$i];
				switch( $c ){
					case '0':  $s += 1;  break;
					case '1':  $s += 0;  break;
					case '2':  $s += 5;  break;
					case '3':  $s += 7;  break;
					case '4':  $s += 9;  break;
					case '5':  $s += 13;  break;
					case '6':  $s += 15;  break;
					case '7':  $s += 17;  break;
					case '8':  $s += 19;  break;
					case '9':  $s += 21;  break;
					case 'A':  $s += 1;  break;
					case 'B':  $s += 0;  break;
					case 'C':  $s += 5;  break;
					case 'D':  $s += 7;  break;
					case 'E':  $s += 9;  break;
					case 'F':  $s += 13;  break;
					case 'G':  $s += 15;  break;
					case 'H':  $s += 17;  break;
					case 'I':  $s += 19;  break;
					case 'J':  $s += 21;  break;
					case 'K':  $s += 2;  break;
					case 'L':  $s += 4;  break;
					case 'M':  $s += 18;  break;
					case 'N':  $s += 20;  break;
					case 'O':  $s += 11;  break;
					case 'P':  $s += 3;  break;
					case 'Q':  $s += 6;  break;
					case 'R':  $s += 8;  break;
					case 'S':  $s += 12;  break;
					case 'T':  $s += 14;  break;
					case 'U':  $s += 16;  break;
					case 'V':  $s += 10;  break;
					case 'W':  $s += 22;  break;
					case 'X':  $s += 25;  break;
					case 'Y':  $s += 24;  break;
					case 'Z':  $s += 23;  break;
				}
			}
			if( chr($s%26 + ord('A')) != $cf[15] ){
				$this->error = 'not valid';
				return false;
			}
			
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

