<?php
namespace marcoc\input\Validator;

interface ValidatorInterface
{	
	/**
	 * 
	 * @param mixed $value
	 * @return bool
	 */
	public function isValid($value);
	
	/**
	 * @return string error code as string
	 */
	public function getError();
}

