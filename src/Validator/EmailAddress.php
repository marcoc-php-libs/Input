<?php
namespace marcoc\input\Validator;

class EmailAddress extends AbstractValidator implements ValidatorInterface
{
	/**
	 * @deprecated
	 */
	CONST ATTR_STRICT = 'strict';
	CONST ATTR_MX_CHECK = 'mx_check';
	CONST ATTR_DEEP_MX_CHECK = 'deep_mx_check';
	
	protected $mx_check = false;
	protected $deep_mx_check = false;
	
	private $error;
	
	/**
	 * The following additional option keys are supported:
     * 'mx_check'        => If MX check should be enabled, boolean
     * 'deep_mx_check'    => If a deep MX check should be done, boolean
     * 
	 * @param array $options
	 */
	public function __construct( array $options = [] )
	{
		$this->mx_check = $options['mx_check'] ?? $this->mx_check;
		$this->deep_mx_check = $options['deep_mx_check'] ?? $this->deep_mx_check;
	}
	
	public function isValid( $value )
	{
		if( ! is_string($value) ){
			$this->error = 'not valid';
			return false;
		}
		
		$parts = explode('@',$value);

		if( sizeof($parts) !== 2 ){
			$this->error = 'not valid';
			return false;
		}
		list( $user_part , $host_part ) = $parts;

		if( ! filter_var($value,FILTER_VALIDATE_EMAIL) ){
			$this->error = 'not valid';
			return false;
		}

		if( ! $this->mx_check ){
			return true;
		}

		$mx_hosts = [];
		$result = getmxrr($host_part, $mx_hosts);

		if( ! $result ){
			$mx_hosts = gethostbynamel($host_part);
		}

		if( ! $mx_hosts ){
			$this->error = 'host not valid';
			return false;
		}

		if( ! $this->deep_mx_check ){
			return true;
		}

		foreach( $mx_hosts as $mx_host ){
			if( filter_var($mx_host,FILTER_VALIDATE_IP) ){
				$mx_ip = $mx_host;
			} else {
				$mx_ip = gethostbyname($mx_host);
			}

			if( ! filter_var($mx_ip,FILTER_VALIDATE_IP,FILTER_FLAG_NO_PRIV_RANGE|FILTER_FLAG_NO_RES_RANGE) ){
				$this->error = 'ip private or reserved';
				return false;
			}
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

