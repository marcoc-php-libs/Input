<?php
namespace marcoc\input\Validator;

class IsUploadedFile implements ValidatorInterface
{	
	private $error;
	
	public function isValid( $value )
	{
		if( ! is_uploaded_file($value) ){
			$this->error = 'not_uploaded_file';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

