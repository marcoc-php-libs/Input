<?php
namespace marcoc\input\Validator;

class PostCode implements ValidatorInterface
{
	CONST ATTR_ALLOWED_COUNTRIES = 'allowed_countries';
	
	protected $allowed_countries;

	protected $error;

	protected $postcode_map;

	/**
	 * @var array|boolean|null
	 * @description last execution detected countries. Contain an array of country codes 2 letter uppercase.
	 * If isValid() has never been called this value is null. If last execution has return false, this value
	 * is false.
	 */
	public $detected_countries;
	
	/**
	 * 
	 * @param array $haystack of allowed myme types
	 */
	public function __construct( array $options = [] )
	{
		if( isset($options['allowed_countries']) ){
			$this->setAllowedCountries( $options['allowed_countries'] );
		}
	}

	public function setAllowedCountries( array $allowed_countries )
	{
		if( ! $allowed_countries ){
			throw new \InvalidArgumentException('allowed_countries must be not empty array');
		}

		$postcode_map = $this->getPostcodeMap();

		$this->allowed_countries = [];
		foreach( $allowed_countries as $country ){
			$country = strtoupper($country);
			if( ! isset( $postcode_map[ $country ] ) ){
				throw new \InvalidArgumentException("Country $country not exists");
			}
			$this->allowed_countries[] = $country;
		}

		return $this;
	}

	public function getAllowedCountries()
	{
		return $this->allowed_countries;
	}
	
	public function isValid( $value )
	{
		$this->detected_countries = false;

		if( ! is_string($value) ){
			$this->error = 'vartype';
			return false;
		}
		
		$postcode_map = $this->getPostcodeMap();
		$is_valid = false;

		foreach( $postcode_map as $country => $regex ){
			if( $this->allowed_countries && ! in_array($country,$this->allowed_countries) ){
				continue;
			}

			if( ! $regex ){
				$regex = '[A-Za-z0-9\- \/]{1},10';
			}

			if( ! preg_match("/^$regex$/",$value)){
				continue;
			}

			$is_valid = true;
			$this->detected_countries[] = $country;
		}

		if( ! $is_valid ){
			$this->error = 'not_valid';
			return false;
		}

		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}

	private function getPostcodeMap()
	{
		if( ! $this->postcode_map ){
			$this->postcode_map = array_map(function($value){
				return $value['post_code']['pcre_regex'] ?? null;
			},\marcoc\input\i18nDb::getDb());
		}

		return $this->postcode_map;
	}
}

