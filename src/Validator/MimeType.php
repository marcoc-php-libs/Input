<?php
namespace marcoc\input\Validator;

class MimeType implements ValidatorInterface
{
	private $haystack;
	
	private $error;
	
	/**
	 * 
	 * @param array $haystack of allowed myme types
	 */
	public function __construct( array $haystack )
	{
		$this->haystack = $haystack;
	}
	
	public function isValid( $value )
	{
		if( ! is_file($value) ){
			$this->error = 'file_not_exists';
			return false;
		}
		
		$content_type = mime_content_type($value);
		
		if( ! $content_type ){
			$this->error = 'mime_not_detected';
			return false;
		}
		
		if( ! in_array($content_type, $this->haystack) ){
			$this->error = 'not_in';
			return false;
		}

		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

