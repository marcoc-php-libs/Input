<?php
namespace marcoc\input\Validator;

class Ip extends AbstractValidator implements ValidatorInterface
{
	CONST ATTR_ALLOW_V4 = 'allow_v4';
	CONST ATTR_ALLOW_V6 = 'allow_v6';
	CONST ATTR_ALLOW_PRIVATE = 'allow_private';
	CONST ATTR_ALLOW_RESERVED = 'allow_reserved';
	
	protected $allow_v4 = true;
	protected $allow_v6 = true;
	protected $allow_private = true;
	protected $allow_reserved = true;
	
	private $error;
	
	/**
	 * The following additional option keys are supported:
     * 'allow_v4' => boolean default true
     * 'allow_v6' => boolean default true
     * 'allow_private' => boolean default true
     * 'allow_reserved' => boolean default true
     * 
	 * @param array $options
	 */
	public function __construct( array $options = [] )
	{
		$this->allow_v4 = $options['allow_v4'] ?? $this->allow_v4;
		$this->allow_v6 = $options['allow_v6'] ?? $this->allow_v6;
		$this->allow_private = $options['allow_private'] ?? $this->allow_private;
		$this->allow_reserved = $options['allow_reserved'] ?? $this->allow_reserved;
		
		if( ! $this->allow_v4 && ! $this->allow_v6 ){
			throw new \Exception('allow_v4 and allow_v6 cannot be both false');
		}
	}
	
	public function isValid( $value )
	{
		$flags = 0;
		if( ! $this->allow_v4 ){
			$flags|=FILTER_FLAG_IPV6;
		} elseif( ! $this->allow_v6 ) {
			$flags|=FILTER_FLAG_IPV4;
		}
		
		if( ! $this->allow_private ){
			$flags|=FILTER_FLAG_NO_PRIV_RANGE;
		}
		if( ! $this->allow_reserved ){
			$flags|=FILTER_FLAG_NO_RES_RANGE;
		}
		
		if( filter_var($value,FILTER_VALIDATE_IP,$flags) === false ){
			$this->error = 'not valid';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

