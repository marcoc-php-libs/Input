<?php
namespace marcoc\input\Validator;

class Type implements ValidatorInterface
{
	
	CONST PHP_NUMERIC = 'PHP_NUMERIC';
	CONST INT = 'INT';
	CONST FLOAT = 'FLOAT';
	CONST STRING = 'STRING';
	CONST BOOLEAN = 'BOOLEAN';
	CONST ARRAY = 'ARRAY';
	CONST OBJECT = 'OBJECT';
	CONST SCALAR = 'SCALAR';
	CONST RESOURCE = 'RESOURCE';
	CONST NULL = 'NULL';
	
	CONST CTYPE_ALNUM = 'CTYPE_ALNUM';
	CONST CTYPE_ALPHA = 'CTYPE_ALPHA';
	CONST CTYPE_CNTRL = 'CTYPE_CNTRL';
	CONST CTYPE_DIGIT = 'CTYPE_DIGIT';
	CONST CTYPE_GRAPH = 'CTYPE_GRAPH';
	CONST CTYPE_LOWER = 'CTYPE_LOWER';
	CONST CTYPE_PRINT = 'CTYPE_PRINT';
	CONST CTYPE_PUNCT = 'CTYPE_PUNCT';
	CONST CTYPE_SPACE = 'CTYPE_SPACE';
	CONST CTYPE_UPPER = 'CTYPE_UPPER';
	CONST CTYPE_XDIGIT = 'CTYPE_XDIGIT';
	
	private $types;
	
	private $error;
	
	/**
	 * 
	 * @param array $types almost one in array must be true
	 */
	public function __construct( array $types )
	{
		$this->types = $types;
	}
	
	public function isValid( $value )
	{
		foreach( $this->types as $type ){
			
			if( $type === self::PHP_NUMERIC ){
				if( is_numeric($value) ) return true;
			} elseif( $type === self::INT ){
				if( is_int($value) ) return true;
			} elseif( $type === self::FLOAT ){
				if( is_float($value) ) return true;
			} elseif( $type === self::STRING ){
				if( is_string($value) ) return true;
			} elseif( $type === self::BOOLEAN ){
				if( is_bool($value) ) return true;
			} elseif( $type === self::ARRAY ){
				if( is_array($value) ) return true;
			} elseif( $type === self::OBJECT ){
				if( is_object($value) ) return true;
			} elseif( $type === self::SCALAR ){
				if( is_scalar($value) ) return true;
			} elseif( $type === self::RESOURCE ){
				if( is_resource($value) ) return true;
			} elseif( $type === self::NULL ){
				if( is_null($value) ) return true;
			} elseif( $type === self::CTYPE_ALNUM ){
				if( ctype_alnum($value) ) return true;
			} elseif( $type === self::CTYPE_ALPHA ){
				if( ctype_alpha($value) ) return true;
			} elseif( $type === self::CTYPE_CNTRL ){
				if( ctype_cntrl($value) ) return true;
			} elseif( $type === self::CTYPE_DIGIT ){
				if( ctype_digit($value) ) return true;
			} elseif( $type === self::CTYPE_GRAPH ){
				if( ctype_graph($value) ) return true;
			} elseif( $type === self::CTYPE_LOWER ){
				if( ctype_lower($value) ) return true;
			} elseif( $type === self::CTYPE_PRINT ){
				if( ctype_print($value) ) return true;
			} elseif( $type === self::CTYPE_PUNCT ){
				if( ctype_punct($value) ) return true;
			} elseif( $type === self::CTYPE_SPACE ){
				if( ctype_space($value) ) return true;
			} elseif( $type === self::CTYPE_UPPER ){
				if( ctype_upper($value) ) return true;
			} elseif( $type === self::CTYPE_XDIGIT ){
				if( ctype_xdigit($value) ) return true;
			}
			
		}
		
		$this->error = 'vartype';
		
		return false;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

