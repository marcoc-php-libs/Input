<?php
namespace marcoc\input\Validator;

class IsDir implements ValidatorInterface
{	
	private $error;
	
	public function isValid( $value )
	{
		if( ! is_dir($value) ){
			$this->error = 'dir_not_exists';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

