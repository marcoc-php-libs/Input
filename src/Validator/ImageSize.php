<?php
namespace marcoc\input\Validator;

class ImageSize implements ValidatorInterface
{
	private $min_w;
	private $max_w;
	private $min_h;
	private $max_h;
	
	private $error;
	
	/**
	 * @param array $options min_width & max_width & min_height & max_height
	 */
	public function __construct( array $options )
	{
		$this->min_w = $options['min_width'] ?? null;
		$this->max_w = $options['max_width'] ?? null;
		$this->min_h = $options['min_height'] ?? null;
		$this->max_h = $options['max_height'] ?? null;
	}
	
	public function isValid( $value )
	{
		if( ! is_file($value) ){
			$this->error = 'file_not_exists';
			return false;
		}
		
		$real_size = getimagesize($value);
		
		if( ! $real_size ){
			$this->error = 'file_not_image';
			return false;
		}
		
		$w = $real_size[0];
		$h = $real_size[1];
		
		if( $this->min_w && $w < $this->min_w ){
			$this->error = 'width_too_small';
			return false;
		}
		
		if( $this->max_w && $w > $this->max_w ){
			$this->error = 'width_too_big';
			return false;
		}
		
		if( $this->min_h && $h < $this->min_h ){
			$this->error = 'height_too_small';
			return false;
		}
		
		if( $this->max_h && $h > $this->max_h ){
			$this->error = 'height_too_big';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

