<?php
namespace marcoc\input\Validator;

class Hostname extends AbstractValidator implements ValidatorInterface
{
	
	CONST ATTR_ALLOW_IP = 'allow_ip';
	CONST ATTR_CHECK_DNS = 'check_dns';
	CONST ATTR_CHECK_IP_OPTIONS = 'check_ip_options';
	
	protected $allow_ip = true;
	protected $check_dns = false;
	protected $check_ip_options = [];
	
	private $error;
	
	/**
	 * The following additional option keys are supported:
     * 'allow_ip' => boolean default true
     * 'check_dns' => perform dns lookup
     * 'check_ip_options' => arrar of options of ip validator. Default empty ( validator not run )
     * 
	 * @param array $options
	 */
	public function __construct( array $options = [] )
	{
		$this->allow_ip = $options['allow_ip'] ?? $this->allow_ip;
		$this->check_dns = $options['check_dns'] ?? $this->check_dns;
		$this->check_ip_options = $options['check_ip_options'] ?? $this->check_ip_options;
		
		if( $this->check_ip_options && ! $this->check_dns ){
			throw new \Exception('if check_ip_options is enabled, check_dns must be anabled too');
		}
	}
	
	public function isValid( $value )
	{
		if( filter_var($value,FILTER_VALIDATE_IP) !== false ){
			if( ! $this->allow_ip ){
				$this->error = 'not valid';
				return false;
			}
			$ip = $value;
			goto skip_dns_check;
		} elseif( filter_var($value,FILTER_VALIDATE_DOMAIN,FILTER_FLAG_HOSTNAME) === false ){
			$this->error = 'not valid';
			return false;
		}
		
		if( $this->check_dns ){
			$ip = gethostbyname($value);
			if( $ip === $value ){
				$this->error = 'dns error';
				return false;
			}
		}
		
		skip_dns_check:
		
		if( $this->check_ip_options ){
			$IpValidator = new Ip( $this->check_ip_options );
			if( ! $IpValidator->isValid($ip) ){
				$this->error = $IpValidator->getError();
				return false;
			}
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

