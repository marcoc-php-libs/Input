<?php
namespace marcoc\input\Validator;

class File implements ValidatorInterface
{
	private $min_size;
	private $max_size;
	
	private $error;
	
	/**
	 * 
	 * @param int $min_size
	 * @param int $max_size
	 */
	public function __construct( $min = null , $max = null )
	{
		$this->min_size = $min;
		$this->max_size = $max;
	}
	
	public function isValid( $value )
	{
		if( ! is_file($value) ){
			$this->error = 'file_not_exists';
			return false;
		}
		
		if( $this->min_size || $this->max_size ){
			$filesize = filesize($value);
			
			if( $this->min_size && $filesize < $this->min_size ){
				$this->error = 'file_too_small';
				return false;
			}
			
			if( $this->max_size && $filesize > $this->max_size ){
				$this->error = 'file_too_big';
				return false;
			}
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

