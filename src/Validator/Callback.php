<?php
namespace marcoc\input\Validator;

class Callback implements ValidatorInterface
{
	
	private $Callback;
	
	private $error;
	
	/**
	 * @param callable $Callback
	 */
	public function __construct( callable $Callback )
	{
		$this->Callback = $Callback;
	}
	
	public function isValid( $value )
	{
		$Callback = $this->Callback;
		return $Callback( $value , $this );
	}
	
	public function setError( string $error )
	{
		$this->error = $error;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

