<?php
namespace marcoc\input\Validator;

class IsJson implements ValidatorInterface
{
	private $error;
	
	private $max_depth;
	
	public function __construct( int $max_depth = null)
	{
		$this->max_depth = $max_depth;
	}
	
	public function isValid( $value )
	{
		if( isset($this->max_depth) ){
			json_decode($value,false,$this->max_depth);
		} else {
			json_decode($value,false);
		}
		
		if( json_last_error() ){
			$this->error = 'not json';
			return false;
		}
		
		return true;
	}
	
	public static function valid( $value )
	{
		$object = new self;
		return $object->isValid($value);
	}
	
	public function getError()
	{
		return $this->error;
	}
}

