<?php
namespace marcoc\input\Validator;

class In implements ValidatorInterface
{	
	private $haystack;
	private $strict = false;
	
	private $error;
	
	/**
	 * 
	 * @param array $haystack array where in_array() run search
	 * @param string $strict strict search setted in in_array( array , search )
	 */
	public function __construct( array $haystack , $strict = false )
	{
		$this->haystack = $haystack;
		$this->strict = $this->strict;
	}
	
	public function isValid( $value )
	{
		if( ! in_array($value, $this->haystack , $this->strict) ){
			$this->error = 'not_in';
			return false;
		}
		
		return true;
	}
	
	public function getError()
	{
		return $this->error;
	}
}

