<?php
namespace Tests\Filter;

use marcoc\input\Filter\DateTime;

class DateTimeTest extends \PHPUnit\Framework\TestCase
{
	function testMain()
	{
		$Filter = new DateTime(['Y-m-d','d-m-Y']);

		$isValid = null;
		$DT = $Filter->filter('2021-09-27',$isValid);
		$this->assertTrue($isValid,print_r(\DateTime::getLastErrors(),true));
		$this->assertInstanceOf(\DateTime::class,$DT);
		
		$isValid = null;
		$DT = $Filter->filter('27-09-2021',$isValid);
		$this->assertTrue($isValid,print_r(\DateTime::getLastErrors(),true));
		$this->assertInstanceOf(\DateTime::class,$DT);

		$isValid = null;
		$DT = $Filter->filter('2021-21-10',$isValid);
		$this->assertFalse($isValid);
		$this->assertIsString($DT);
	}
}

