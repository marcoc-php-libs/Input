<?php
namespace Tests\Filter;

use marcoc\input\Filter\Trim;

class TrimTest extends \PHPUnit\Framework\TestCase
{
	function testMain()
	{
		$Filter = new Trim();
		$this->assertEquals('sss', $Filter->filter("  sss "));
		$this->assertEquals('sss', $Filter->filter("\n\tsss\r"));
		
		$Filter = new Trim(null,Trim::DIRECTION_L);
		$this->assertEquals('sss ', $Filter->filter("  sss "));
		$this->assertEquals("sss\r", $Filter->filter("\n\tsss\r"));
		
		$Filter = new Trim(null,Trim::DIRECTION_R);
		$this->assertEquals('  sss', $Filter->filter("  sss "));
		$this->assertEquals("\n\tsss", $Filter->filter("\n\tsss\r"));
		
		$Filter = new Trim(' ');
		$this->assertEquals('sss', $Filter->filter("  sss "));
		$this->assertEquals("\n \tsss\r", $Filter->filter("\n \tsss\r "));
	}
}

