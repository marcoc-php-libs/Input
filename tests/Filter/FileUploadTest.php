<?php
namespace Tests\Filter;

use marcoc\input\Filter\FileUpload;

class FileUploadTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @dataProvider provideValid
	 */
	function testValid( $args , $input , $expected )
	{
		$is_valid = null;
		
		$Filter = new FileUpload( $args );
		if( $expected === null ){
			$this->assertNull( $Filter->filter($input , $is_valid) );
		} elseif( $expected === 'file://' ){
			$this->assertFileExists( $Filter->filter($input , $is_valid) );
		}
		$this->assertTrue( $is_valid );
	}
	
	function provideValid()
	{
		return [
			[
				[],'',null
			],
			[
				[FileUpload::ATTR_ALLOW_BASE64=>true],'dGVzdA==','file://'
			],
		];
	}
	
	/**
	 * @dataProvider provideFailure
	 */
	function testFailure( $args , $input )
	{
		$is_valid = null;
		
		$Filter = new FileUpload( $args );
		$Filter->filter($input , $is_valid);
		$this->assertFalse( $is_valid );
	}
	
	function provideFailure()
	{
		return [
			[
				[FileUpload::ATTR_SET_NULL_IF_NOT_SEND=>false],''
			],
			[
				[],'dmFsaWQgYmFzZTY0' // valid base64
			],
			[
				[FileUpload::ATTR_ALLOW_BASE64=>true],'not:valid:base64'
			],
			[
				[],[
					'tmp_name' => tempnam(sys_get_temp_dir(), 'phpunit test') // valid file
				]
			],
			[
				[],100
			]
		];
	}
}

