<?php



use marcoc\input\Filter\PhoneNumber;

class PhoneNumberTest extends \PHPUnit\Framework\TestCase
{
	function testMain()
	{
		$default_combination = [ PhoneNumber::TYPE_MOBILE , PhoneNumber::TYPE_FIXED_LINE , PhoneNumber::TYPE_FIXED_LINE_OR_MOBILE ];
		$any_mask = 32767;
		$combinations = [
			[],
			[ PhoneNumber::TYPE_MOBILE ],
			[ PhoneNumber::TYPE_FIXED_LINE ],
			[ PhoneNumber::TYPE_EMERGENCY ],
			'*'
		];
		
		$input_data = [];
		
		foreach( get_class_methods($this) as $method ){
			if( strstr($method, 'getInput') !== false ){
				$input_data = array_merge($input_data , call_user_func([$this,$method]) );
			}
		}
		
		foreach ($combinations as $idx_comb => $combination ){
			
			$set_attr_types = true;
			
			if( $combination === '*'){
				$mask = $any_mask;
				$any = true;
			} else {
				if( ! $combination ){
					$combination = $default_combination;
					$set_attr_types = false;
				}
				
				$mask = 0;
				foreach ($combination as $type){
					$mask = $mask | $type;
				}
			}
			
			$base_options = [];
			if( $set_attr_types ){
				$base_options[PhoneNumber::ATTR_ALLOW_TYPES] = $mask;
			}
			
			foreach( $input_data as $idx => $input ){
				$number = $input[0];
				$default_country = $input[1];
				$number_type = $input[2];
				$is_valid = $input[3];
				
				if( $is_valid && $combination !== '*' && ! in_array($number_type, $combination) ){
					$is_valid = false;
				}
				
				$options = $base_options;
				if( $default_country ){
					$options[PhoneNumber::ATTR_DEFAULT_COUNTRY] = $default_country;
				}
				
				$ObjIntance = new PhoneNumber( $options );
				
				$is_valid_run = null;
				$ObjIntance->filter($number , $is_valid_run);
				
				$err_mssg = "comb: $idx_comb, idx: $idx, Number: $number, Type: $number_type";
				if( $ObjIntance->getError() ){
					$err_mssg.= ' Err: '.$ObjIntance->getError();
				}
				
				if( $is_valid ){
					$this->assertTrue( $is_valid_run ,  $err_mssg );
				} else {
					$this->assertFalse( $is_valid_run ,  $err_mssg );
				}
			}
		}
	}
	
	function getInputMobile()
	{
		$type = PhoneNumber::TYPE_MOBILE;
		
		return [
			['+393771315046' , null , $type , true],
			['393771315046' , null , $type , true],
			['39 377 13 15046' , null , $type , true],
			['3771315046' , 'IT' , $type , true],
			// false
			['' , null , $type , false],
			['5' , null , $type , false],
			['a' , null , $type , false],
			[8 , null , $type , false],
			['+3937713150' , null , $type , false],
			['+393771315' , null , $type , false],
			['393771315' , null , $type , false],
			['3771315046' , null , $type , false],
			['+3937713150' , null , $type , false],
		];
	}
	
	function getInputFixLine()
	{
		$type = PhoneNumber::TYPE_FIXED_LINE;
		
		return [
			['+390331850016' , null , $type , true],
			['390331850016' , null , $type , true],
			['39 0331 8500 16' , null , $type , true],
			['0331 8500 16' , 'it' , $type , true],
			// false
			['+390331 8' , null , $type , false],
			['0331 8500 16' , null , $type , false],
			['0331 8' , 'iT' , $type , false],
		];
	}
	
	/**
	 * 
	 * @dataProvider inputCountryProvider
	 */
	function testCountry( $input , $valid )
	{
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_ALLOW_COUNTRIES => ['IT','ch']
		]);
		
		$is_valid_result = null;
		$ObjIntance->filter($input,$is_valid_result);
		
		if( $valid ){
			$this->assertTrue( $is_valid_result );
		} else {
			$this->assertFalse( $is_valid_result );
		}
	}
	
	function inputCountryProvider()
	{
		return [
			['+39 02-8295-1349 ' , true], // Italy
			['+41 043-508-03-37 ' , true], // Swiss
			['+34 932-20-28-49' , false] // Spain
		];
	}
	
	function testOutput()
	{
		$input = '+39 02-8295-1349';
		
		$ObjIntance = new PhoneNumber();
		$this->assertEquals('+390282951349', $ObjIntance->filter($input) );
		
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_RETURN_TYPE => PhoneNumber::RETURN_UNMODIFIED
		]);
		$this->assertEquals( $input , $ObjIntance->filter($input) );
		
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_RETURN_TYPE => PhoneNumber::RETURN_E164
		]);
		$this->assertEquals( '+390282951349' , $ObjIntance->filter($input) );
		
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_RETURN_TYPE => PhoneNumber::RETURN_INTERNATIONAL
		]);
		$this->assertEquals( '+39 02 8295 1349' , $ObjIntance->filter($input) );
		
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_RETURN_TYPE => PhoneNumber::RETURN_NATIONAL
		]);
		$this->assertEquals( '02 8295 1349' , $ObjIntance->filter($input) );
		
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_RETURN_TYPE => PhoneNumber::RETURN_RFC3966
		]);
		$this->assertEquals( 'tel:+39-02-8295-1349' , $ObjIntance->filter($input) );
		
		$ObjIntance = new PhoneNumber([
			PhoneNumber::ATTR_RETURN_TYPE => PhoneNumber::RETURN_OBJECT
		]);
		$this->assertInstanceOf( \libphonenumber\PhoneNumber::class , $ObjIntance->filter($input) );
	}
}

