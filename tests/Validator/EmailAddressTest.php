<?php

use marcoc\input\Validator\EmailAddress;

class EmailAddressTest extends \PHPUnit\Framework\TestCase
{
	public function testValid()
	{
		$Validator = new EmailAddress();
		
		$this->assertTrue( $Validator->isValid('marco@intoway.net') );
		$this->assertTrue( $Validator->isValid('marco@hostnotvalid.notvalid') );
	}

	/**
	 * @group network
	 */
	public function testMxValid()
	{
		$Validator = new EmailAddress([
			EmailAddress::ATTR_MX_CHECK => true
		]);
		
		$this->assertTrue( $Validator->isValid('marco@intoway.net') );

		$Validator = new EmailAddress([
			EmailAddress::ATTR_MX_CHECK => true,
			EmailAddress::ATTR_DEEP_MX_CHECK => true
		]);
		
		$this->assertTrue( $Validator->isValid('marco@intoway.net') );
	}

	
	/**
	 * @dataProvider additionProviderTestNotValid
	 */
	public function testNotValid( $email )
	{
		$Validator = new EmailAddress();
		
		$this->assertFalse( $Validator->isValid( $email ) );
	}

	/**
	 * @group network
	 */
	public function testMxNotValid()
	{
		$Validator = new EmailAddress([
			EmailAddress::ATTR_MX_CHECK => true
		]);
		
		$this->assertFalse( $Validator->isValid('marco@hostnotvalid.notvalid') );

		$Validator = new EmailAddress([
			EmailAddress::ATTR_MX_CHECK => true,
			EmailAddress::ATTR_DEEP_MX_CHECK => true
		]);
		
		$this->assertFalse( $Validator->isValid('marco@hostnotvalid.notvalid') );
	}
	
	public function additionProviderTestNotValid()
	{
		return [
			['marco@intoway.'],
			['marcointoway.net'],
			['marco at intoway.net'],
			['@intoway.net'],
			[false],
			[4],
			['cacca'],
			['mateo che palle']
		];
	}
}

