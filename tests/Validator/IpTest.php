<?php

use marcoc\input\Validator\Ip;

class IpTest extends \PHPUnit\Framework\TestCase
{
	public function testValid()
	{
		$Validator = new Ip();
		$this->assertTrue( $Validator->isValid('8.8.8.8') );
		
		$Validator = new Ip([
			Ip::ATTR_ALLOW_V6 => false,
			Ip::ATTR_ALLOW_PRIVATE => false,
			Ip::ATTR_ALLOW_RESERVED => false
		]);
		$this->assertTrue( $Validator->isValid('8.8.8.8') );
		
		$Validator = new Ip([
			Ip::ATTR_ALLOW_PRIVATE => false,
		]);
		$this->assertTrue( $Validator->isValid('127.0.0.1') );
		
		$Validator = new Ip([
			Ip::ATTR_ALLOW_RESERVED => false,
		]);
		$this->assertTrue( $Validator->isValid('10.0.0.1') );
		$this->assertTrue( $Validator->isValid('172.16.0.1') );
		$this->assertTrue( $Validator->isValid('192.168.0.1') );
	}
	
	public function testNotValid()
	{
		$Validator = new Ip();
		
		$this->assertFalse( $Validator->isValid( '1.2.3.256' ) );
		
		$Validator = new Ip([
			Ip::ATTR_ALLOW_V4 => false,
		]);
		$this->assertFalse( $Validator->isValid('8.8.8.8') );
		
		$Validator = new Ip([
			Ip::ATTR_ALLOW_PRIVATE => false,
		]);
		$this->assertFalse( $Validator->isValid('10.0.0.0') );
		$this->assertFalse( $Validator->isValid('172.16.0.1') );
		$this->assertFalse( $Validator->isValid('192.168.0.1') );
		
		$Validator = new Ip([
			Ip::ATTR_ALLOW_RESERVED => false,
		]);
		$this->assertFalse( $Validator->isValid('127.0.0.1') );
	}
	

}

