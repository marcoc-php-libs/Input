<?php
namespace Tests\Filter;

use marcoc\input\Validator\IsTheoreticObject;

class IsTheoreticObjectTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @dataProvider dataProviderValid
	 */
	function testValid( $input )
	{
		$Validator = new IsTheoreticObject();
		
		$this->assertTrue( $Validator->isValid($input) , print_r($input,true) );
	}
	
	function dataProviderValid()
	{
		return [
			[	[]	],
			[	[1,2,'3'=>3]	],
			[	[0=>1,1=>2,3=>3]	]
		];
	}
	
	/**
	 * @dataProvider dataProviderNotValid
	 */
	function testNotValid( $input )
	{
		$Validator = new IsTheoreticObject();
		
		$this->assertFalse( $Validator->isValid($input) ,  print_r($input,true) );
	}
	
	function dataProviderNotValid()
	{
		return [
			[	[1,2,3]	],
			[	[0=>1,1=>2,2=>3]	]
		];
	}
}

