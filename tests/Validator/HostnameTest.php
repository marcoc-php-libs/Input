<?php

use marcoc\input\Validator\Hostname;
use marcoc\input\Validator\Ip;

class HostnameTest extends \PHPUnit\Framework\TestCase
{
	public function testMain()
	{
		$Validator = new Hostname();
		$this->assertTrue( $Validator->isValid('localhost') );
		$this->assertTrue( $Validator->isValid('intoway.net') );
		$this->assertTrue( $Validator->isValid('8.8.8.8') );
		$this->assertTrue( $Validator->isValid('notexists.solverdigital.com') );
		
		$Validator = new Hostname([
			Hostname::ATTR_CHECK_DNS => true
		]);
		$this->assertTrue( $Validator->isValid('localhost') );
		$this->assertTrue( $Validator->isValid('intoway.net') );
		$this->assertTrue( $Validator->isValid('8.8.8.8') );
		$this->assertFalse( $Validator->isValid('notexists.solverdigital.com.') );
		
		$Validator = new Hostname([
			Hostname::ATTR_ALLOW_IP => false
		]);
		$this->assertTrue( $Validator->isValid('localhost') );
		$this->assertTrue( $Validator->isValid('intoway.net') );
		$this->assertFalse( $Validator->isValid('8.8.8.8') );
		
		$Validator = new Hostname([
			Hostname::ATTR_CHECK_DNS => true,
			Hostname::ATTR_CHECK_IP_OPTIONS => [
				Ip::ATTR_ALLOW_PRIVATE => false,
				Ip::ATTR_ALLOW_RESERVED => false
			]
		]);
		$this->assertFalse( $Validator->isValid('localhost') );
		$this->assertFalse( $Validator->isValid('127.0.0.1') );
		$this->assertTrue( $Validator->isValid('intoway.net') );
	}
	

}

