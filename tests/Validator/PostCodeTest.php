<?php

use marcoc\input\Validator\PostCode;

class PostCodeTest extends \PHPUnit\Framework\TestCase
{
	public function testValid()
	{
		$Validator = new PostCode();
		$this->assertTrue( $Validator->isValid('12345') );
		$this->assertTrue( in_array('IT',$Validator->detected_countries) );
		
		$Validator = new PostCode([
			PostCode::ATTR_ALLOWED_COUNTRIES => ['IT','ch']
		]);
		$this->assertTrue( $Validator->isValid('12345') );
		$this->assertEquals(['IT'] , $Validator->detected_countries);
		$this->assertTrue( $Validator->isValid('1234') );
		$this->assertEquals(['CH'] , $Validator->detected_countries);
	}
	
	public function testNotValid()
	{
		$Validator = new PostCode();
		$this->assertFalse( $Validator->isValid('12345678910') );
		$this->assertFalse( $Validator->isValid( 21100 ) );
		
		$Validator = new PostCode([
			PostCode::ATTR_ALLOWED_COUNTRIES => ['IT']
		]);
		$this->assertFalse( $Validator->isValid('1234') );
		$Validator = new PostCode([
			PostCode::ATTR_ALLOWED_COUNTRIES => ['CH']
		]);
		$this->assertFalse( $Validator->isValid('12345') );
	}
	

}

