<?php
namespace Tests\Filter;

use marcoc\input\Validator\IsTheoreticArray;

class IsTheoreticArrayTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * @dataProvider dataProviderValid
	 */
	function testValid( $input )
	{
		$Validator = new IsTheoreticArray();
		
		$this->assertTrue( $Validator->isValid($input) , print_r($input,true) );
	}
	
	function dataProviderValid()
	{
		return [
			[	[]	],
			[	[1,2,3]	],
			[	[0=>1,1=>2,2=>3]	]
		];
	}
	
	/**
	 * @dataProvider dataProviderNotValid
	 */
	function testNotValid( $input )
	{
		$Validator = new IsTheoreticArray();
		
		$this->assertFalse( $Validator->isValid($input) ,  print_r($input,true) );
	}
	
	function dataProviderNotValid()
	{
		return [
			[	[1,2,'3'=>3]	],
			[	[0=>1,1=>2,3=>3]	]
		];
	}
}

