<?php

use marcoc\input\Validator\Hostname;
use marcoc\input\Validator\Ip;
use marcoc\input\Validator\Url;
use marcoc\input\PCREString;

class UrlTest extends \PHPUnit\Framework\TestCase
{
	public function testMain()
	{
		$Validator = new Url();
		$this->assertTrue( $Validator->isValid('http://localhost') , $Validator->getError()??'');
		$this->assertTrue( $Validator->isValid('http://www.intoway.net') , $Validator->getError()??'');
		$this->assertTrue( $Validator->isValid('http://8.8.8.8/path') , $Validator->getError()??'');
		$this->assertTrue( $Validator->isValid('http://notexists.intoway.net?q=1#6') , $Validator->getError()??'');
		
		
		$Validator = new Url([
			Url::ATTR_CHECK_HOSTNAME_OPTIONS => [
				Hostname::ATTR_CHECK_DNS => true,
				Hostname::ATTR_CHECK_IP_OPTIONS => [
					Ip::ATTR_ALLOW_PRIVATE => false,
					Ip::ATTR_ALLOW_RESERVED => false
				]
			]
		]);
		$this->assertTrue( $Validator->isValid('http://intoway.net') , $Validator->getError()??'');
		$this->assertTrue( $Validator->isValid('http://8.8.8.8') , $Validator->getError()??'');
		$this->assertFalse( $Validator->isValid('http://localhost') );
		
		$Validator = new Url([
			Url::ATTR_ALLOWED_SCHEME => ['https']
		]);
		$this->assertFalse( $Validator->isValid('localhost') );
		$this->assertTrue( $Validator->isValid('https://localhost') );
		$this->assertFalse( $Validator->isValid('http://localhost') );
		$this->assertFalse( $Validator->isValid('ftp://localhost') );
		$Validator = new Url([
			Url::ATTR_ALLOWED_SCHEME => new PCREString('/http(s)?/')
		]);
		$this->assertTrue( $Validator->isValid('http://intoway.net') , $Validator->getError()??'');
		$this->assertFalse( $Validator->isValid('ftp://intoway.net') , $Validator->getError()??'');
		
		
		$Validator = new Url([
			Url::ATTR_ALLOWED_PATH => new PCREString('/^\/prefix.*$/')
		]);
		$this->assertTrue( $Validator->isValid('http://localhost/prefix') );
		$this->assertTrue( $Validator->isValid('http://localhost/prefix/andother') );
		$this->assertFalse( $Validator->isValid('http://localhost/other') );
		
		
		$Validator = new Url([
			Url::ATTR_ALLOWED_PORT => true
		]);
		$this->assertFalse( $Validator->isValid('localhost') );
		$Validator->allowed_port = false;
		$this->assertFalse( $Validator->isValid('localhost:8080') );
		$Validator->allowed_port = new PCREString('/http(s)?/');
		$this->assertFalse( $Validator->isValid('ftp://localhost:8080') );
		
		$Validator = new Url([
			Url::ATTR_ALLOWED_QUERY => false
		]);
		$this->assertFalse( $Validator->isValid('http://localhost?q=1') );
		$this->assertTrue( $Validator->isValid('http://localhost') );
		$Validator = new Url([
			Url::ATTR_ALLOWED_QUERY => true
		]);
		$this->assertTrue( $Validator->isValid('http://localhost?q=1') );
		$this->assertFalse( $Validator->isValid('http://localhost') );
		
	}
	

}

