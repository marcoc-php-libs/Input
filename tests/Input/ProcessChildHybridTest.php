<?php

use marcoc\input\Input;
use marcoc\input\Validator\Equal;
use marcoc\input\Validator\Not;
use marcoc\input\Validator\Str;
use marcoc\input\Validator\Type;

/**
 * @description this test the fix of issue #3 (Using process as array is impossible check that array is not empty) 
 */
class ProcessChildHybridTest extends \PHPUnit\Framework\TestCase
{
	public function testRun()
	{
		$Input = new Input('ROOT', true,[
			new Type([Type::STRING]),
			new Str(null,null,3)
		]);
		$Input->process_as_array = Input::PROCESS_AS_ARRAY_ALWAYS;
		
		$this->assertTrue( $Input->setData([])->isValid() ); // should not be true
		$this->assertTrue( $Input->setData(['abc','def'])->isValid() );

		$Input = new Input('ROOT', true,[
			new Type([Type::ARRAY]),
			new Not( new Equal([]) ),
			new Str(null,null,3)
		]);
		$Input->process_as_array = Input::PROCESS_AS_ARRAY_ALWAYS;
		$Input->process_as_array_from_child_index = 2;
		
		$this->assertFalse( $Input->setData([])->isValid() );
		$this->assertTrue( $Input->setData(['abc','def'])->isValid() );
	}
}

