<?php

use marcoc\input\Input;
use marcoc\input\Validator\IsTheoreticArray;
use marcoc\input\Validator\Str;

/**
 * @description this test show how solve problem described in issue #2 (Input::isValid() return true if setData() param is null) 
 */
class InputError extends \PHPUnit\Framework\TestCase
{
	public function testRun()
	{
		$Input = new Input('ROOT', true);
		$Input->addChild(new Input('values',true,[
			new IsTheoreticArray(),
			new Str(3,5)
		]));
		$Input->get('values')->process_as_array = Input::PROCESS_AS_ARRAY_ALWAYS;
		$Input->get('values')->process_as_array_from_child_index = 1;

		$Input->addChild(new Input('childs',true));
		$Input->get('childs')->addChild(new Input('abc',true,[
			new Str(3,5)
		]));
		
		$this->assertFalse( $Input->setData([
			'values' => [
				'a' => '123'
			]
		])->isValid() );
		$this->assertEquals('ROOT.values',$Input->Errors->list[0]->name);

		$this->assertFalse( $Input->setData([
			'values' => [
				'123',
				'12'
			]
		])->isValid() );
		$this->assertEquals('ROOT.values.1',$Input->Errors->list[0]->name);

		$this->assertFalse( $Input->setData([
			'values' => [
				'123',
				'124'
			],
			'childs' => [
				'abc' => '12'
			]
		])->isValid() );
		$this->assertEquals('ROOT.childs.abc',$Input->Errors->list[0]->name);

		$this->assertFalse( $Input->setData([
			'values' => [
				'123',
				'124'
			],
			'childs' => [
			]
		])->isValid() );
		$this->assertEquals('ROOT.childs.abc',$Input->Errors->list[0]->name);

		$Input->get('childs')->process_childs_as_array = Input::PROCESS_AS_ARRAY_ALWAYS;

		$this->assertFalse( $Input->setData([
			'values' => [
				'123',
				'124'
			],
			'childs' => [
				'f',
				[
					'abc' => '123'
				],
				[
					'agf' => 1
				]
			]
		])->isValid() );
		$this->assertEquals('ROOT.childs.0',$Input->Errors->list[0]->name );

		$this->assertFalse( $Input->setData([
			'values' => [
				'123',
				'124'
			],
			'childs' => [
				[
					'abc' => '123456'
				]
			]
		])->isValid() );
		$this->assertEquals('ROOT.childs.0.abc',$Input->Errors->list[0]->name );
	}
}

