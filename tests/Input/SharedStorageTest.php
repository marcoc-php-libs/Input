<?php

use marcoc\input\Input;
use marcoc\input\Filter\Callback as FilterCallback;

class SharedStorageTest extends \PHPUnit\Framework\TestCase
{
	public function testRun()
	{
		$_this = $this;
		
		$Input = new Input('ROOT', true);
		$Input->addToChain(
			new FilterCallback(function($value)use($Input){
				$Input->shared_storage['test_4'] = 4;
				$SharedStorage = $Input->getSharedStorage();
				$SharedStorage['test_4'] = 4;
				return $value;
			})	
		);
		
		$InputChild = new Input('child', false);
		$InputChild->addToChain(
			new FilterCallback(function($value)use($InputChild , $_this){
				$_this->assertEquals(4, $InputChild->shared_storage['test_4']);
				$_this->assertEquals(4, $InputChild->getSharedStorage()['test_4']);
			})
		);
		$Input->addChild($InputChild);
		
		$Input->setData([
			'child' => 'string'
		]);
		
		$Input->isValid();
	}
}

