<?php

use marcoc\input\Input;
use marcoc\input\Validator\Str;

/**
 * @description this test show how solve problem described in issue #2 (Input::isValid() return true if setData() param is null) 
 */
class InputNullTest extends \PHPUnit\Framework\TestCase
{
	public function testRun()
	{
		$Input = new Input('ROOT', true);
		$Input->addChild(new Input('value',true,[
			new Str(1,100)
		]));
		$Input->stop_if_null = false;
		
		$this->assertFalse( $Input->setData(null)->isValid() );
	}
}

