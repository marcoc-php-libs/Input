<?php

use marcoc\input\Input;
use marcoc\input\Filter\Callback as FilterCallback;

class SetAllNotRequiredTest extends \PHPUnit\Framework\TestCase
{
	/**
	 * 
	 * @dataProvider dataProvider
	 */
	public function testRun( $depth , $expected )
	{
		$_this = $this;
		
		$Input = new Input('ROOT', true);
		
		$Child_1 = new Input('child_1',true);
		
		$Child_2 = new Input('child_2',true);
		$Child_2->addChild(new Input('Child_2_subchild', true));
		
		$Input->addChild($Child_1)->addChild($Child_2);
		
		$Input->setAllNotRequired( $depth );
		
		$this->assertEquals($expected, [
			'ROOT' => $Input->required,
			'child_1' => $Input->get('child_1')->required,
			'child_2' => $Input->get('child_2')->required,
			'child_2.Child_2_subchild' => $Input->get('child_2')->get('Child_2_subchild')->required,
		]);
	}
	
	public function dataProvider()
	{
		return [
			[
				0,
				[
					'ROOT' => true,
					'child_1' => true,
					'child_2' => true,
					'child_2.Child_2_subchild' => true,
				]
			],
			[
				1,
				[
					'ROOT' => true,
					'child_1' => false,
					'child_2' => false,
					'child_2.Child_2_subchild' => true,
				]
			],
			[
				2,
				[
					'ROOT' => true,
					'child_1' => false,
					'child_2' => false,
					'child_2.Child_2_subchild' => false,
				]
			],
			[
				-1,
				[
					'ROOT' => true,
					'child_1' => false,
					'child_2' => false,
					'child_2.Child_2_subchild' => false,
				]
			],
		];
	}
	
	
}

